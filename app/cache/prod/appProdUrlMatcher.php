<?php

use Symfony\Component\Routing\Exception\MethodNotAllowedException;
use Symfony\Component\Routing\Exception\ResourceNotFoundException;
use Symfony\Component\Routing\RequestContext;

/**
 * appProdUrlMatcher
 *
 * This class has been auto-generated
 * by the Symfony Routing Component.
 */
class appProdUrlMatcher extends Symfony\Bundle\FrameworkBundle\Routing\RedirectableUrlMatcher
{
    /**
     * Constructor.
     */
    public function __construct(RequestContext $context)
    {
        $this->context = $context;
    }

    public function match($pathinfo)
    {
        $allow = array();
        $pathinfo = rawurldecode($pathinfo);
        $context = $this->context;
        $request = $this->request;

        // blog_login
        if ($pathinfo === '/login') {
            return array (  '_controller' => 'Common\\UserBundle\\Controller\\LoginController::loginAction',  '_route' => 'blog_login',);
        }

        // user_resetPassword
        if (0 === strpos($pathinfo, '/reset-password') && preg_match('#^/reset\\-password/(?P<actionToken>[^/]++)$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'user_resetPassword')), array (  '_controller' => 'Common\\UserBundle\\Controller\\LoginController::resetPasswordAction',));
        }

        if (0 === strpos($pathinfo, '/account-')) {
            // user_activateAccount
            if (0 === strpos($pathinfo, '/account-activation') && preg_match('#^/account\\-activation/(?P<actionToken>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'user_activateAccount')), array (  '_controller' => 'Common\\UserBundle\\Controller\\LoginController::activateAccountAction',));
            }

            // user_accountSettings
            if ($pathinfo === '/account-settings') {
                return array (  '_controller' => 'Common\\UserBundle\\Controller\\UserController::accountSettingsAction',  '_route' => 'user_accountSettings',);
            }

        }

        if (0 === strpos($pathinfo, '/log')) {
            // _check_path
            if ($pathinfo === '/login-check') {
                return array('_route' => '_check_path');
            }

            // _logout
            if ($pathinfo === '/logout') {
                return array('_route' => '_logout');
            }

        }

        if (0 === strpos($pathinfo, '/a')) {
            if (0 === strpos($pathinfo, '/admin-panel/users')) {
                // user_adminUsersList
                if (0 === strpos($pathinfo, '/admin-panel/users/list') && preg_match('#^/admin\\-panel/users/list(?:/(?P<page>\\d+))?$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'user_adminUsersList')), array (  'page' => 1,  '_controller' => 'Common\\UserBundle\\Controller\\AdminController::indexAction',));
                }

                // user_adminUserForm
                if (0 === strpos($pathinfo, '/admin-panel/users/form') && preg_match('#^/admin\\-panel/users/form/(?P<id>\\d+)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'user_adminUserForm')), array (  '_controller' => 'Common\\UserBundle\\Controller\\AdminController::formAction',));
                }

            }

            // blog_about
            if ($pathinfo === '/about') {
                return array (  '_controller' => 'Symfony\\Bundle\\FrameworkBundle\\Controller\\TemplateController::templateAction',  'template' => 'AirBlogBundle:Pages:about.html.twig',  '_route' => 'blog_about',);
            }

        }

        // blog_contact
        if ($pathinfo === '/contact') {
            return array (  '_controller' => 'Air\\BlogBundle\\Controller\\PagesController::contactAction',  '_route' => 'blog_contact',);
        }

        // blog_index
        if (preg_match('#^/(?P<page>\\d+)?$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'blog_index')), array (  'page' => 1,  '_controller' => 'Air\\BlogBundle\\Controller\\PostsController::indexAction',));
        }

        // blog_search
        if (0 === strpos($pathinfo, '/search') && preg_match('#^/search(?:/(?P<page>\\d+))?$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'blog_search')), array (  'page' => 1,  '_controller' => 'Air\\BlogBundle\\Controller\\PostsController::searchAction',));
        }

        // blog_post
        if (preg_match('#^/(?P<slug>[^/]++)$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'blog_post')), array (  '_controller' => 'Air\\BlogBundle\\Controller\\PostsController::postAction',));
        }

        // blog_deleteComment
        if (0 === strpos($pathinfo, '/post/comment/delete') && preg_match('#^/post/comment/delete/(?P<commentId>[^/]++)/(?P<token>[^/]++)$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'blog_deleteComment')), array (  '_controller' => 'Air\\BlogBundle\\Controller\\PostsController::deleteCommentAction',));
        }

        // blog_category
        if (0 === strpos($pathinfo, '/category') && preg_match('#^/category/(?P<slug>[^/]++)(?:/(?P<page>\\d+))?$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'blog_category')), array (  'page' => 1,  '_controller' => 'Air\\BlogBundle\\Controller\\PostsController::categoryAction',));
        }

        // blog_tag
        if (0 === strpos($pathinfo, '/tag') && preg_match('#^/tag/(?P<slug>[^/]++)(?:/(?P<page>\\d+))?$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'blog_tag')), array (  'page' => 1,  '_controller' => 'Air\\BlogBundle\\Controller\\PostsController::tagAction',));
        }

        if (0 === strpos($pathinfo, '/admin-panel')) {
            // admin_dashboard
            if (rtrim($pathinfo, '/') === '/admin-panel/dashboard') {
                if (substr($pathinfo, -1) !== '/') {
                    return $this->redirect($pathinfo.'/', 'admin_dashboard');
                }

                return array (  '_controller' => 'Air\\AdminBundle\\Controller\\DashboardController::indexAction',  '_route' => 'admin_dashboard',);
            }

            if (0 === strpos($pathinfo, '/admin-panel/categories')) {
                // admin_categoriesList
                if (0 === strpos($pathinfo, '/admin-panel/categories/list') && preg_match('#^/admin\\-panel/categories/list(?:/(?P<page>[^/]++))?$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_categoriesList')), array (  'page' => 1,  '_controller' => 'Air\\AdminBundle\\Controller\\CategoriesController::indexAction',));
                }

                // admin_categoryForm
                if (0 === strpos($pathinfo, '/admin-panel/categories/form') && preg_match('#^/admin\\-panel/categories/form(?:/(?P<id>\\d+))?$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_categoryForm')), array (  'id' => NULL,  'Category' => NULL,  '_controller' => 'Air\\AdminBundle\\Controller\\CategoriesController::formAction',));
                }

                // admin_categoryDelete
                if (0 === strpos($pathinfo, '/admin-panel/categories/delete') && preg_match('#^/admin\\-panel/categories/delete/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_categoryDelete')), array (  '_controller' => 'Air\\AdminBundle\\Controller\\CategoriesController::deleteAction',));
                }

            }

            if (0 === strpos($pathinfo, '/admin-panel/posts')) {
                // admin_postsList
                if (0 === strpos($pathinfo, '/admin-panel/posts/list') && preg_match('#^/admin\\-panel/posts/list(?:/(?P<status>[^/]++)(?:/(?P<page>\\d+))?)?$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_postsList')), array (  'status' => 'all',  'page' => 1,  '_controller' => 'Air\\AdminBundle\\Controller\\PostsController::indexAction',));
                }

                // admin_postForm
                if (0 === strpos($pathinfo, '/admin-panel/posts/form') && preg_match('#^/admin\\-panel/posts/form(?:/(?P<id>\\d+))?$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_postForm')), array (  'id' => NULL,  '_controller' => 'Air\\AdminBundle\\Controller\\PostsController::formAction',));
                }

                // admin_postDelete
                if (0 === strpos($pathinfo, '/admin-panel/posts/delete') && preg_match('#^/admin\\-panel/posts/delete/(?P<id>\\d+)/(?P<token>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_postDelete')), array (  '_controller' => 'Air\\AdminBundle\\Controller\\PostsController::deleteAction',));
                }

            }

            if (0 === strpos($pathinfo, '/admin-panel/tags')) {
                // admin_tagsList
                if (0 === strpos($pathinfo, '/admin-panel/tags/list') && preg_match('#^/admin\\-panel/tags/list(?:/(?P<page>\\d+))?$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_tagsList')), array (  'page' => 1,  '_controller' => 'Air\\AdminBundle\\Controller\\TagsController::indexAction',));
                }

                // admin_tagForm
                if (0 === strpos($pathinfo, '/admin-panel/tags/form') && preg_match('#^/admin\\-panel/tags/form(?:/(?P<id>\\d+))?$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_tagForm')), array (  'id' => NULL,  'Tag' => NULL,  '_controller' => 'Air\\AdminBundle\\Controller\\TagsController::formAction',));
                }

                // admin_tagDelete
                if (0 === strpos($pathinfo, '/admin-panel/tags/delete') && preg_match('#^/admin\\-panel/tags/delete/(?P<id>\\d+)/(?P<token>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_tagDelete')), array (  '_controller' => 'Air\\AdminBundle\\Controller\\TagsController::deleteAction',));
                }

            }

        }

        throw 0 < count($allow) ? new MethodNotAllowedException(array_unique($allow)) : new ResourceNotFoundException();
    }
}
