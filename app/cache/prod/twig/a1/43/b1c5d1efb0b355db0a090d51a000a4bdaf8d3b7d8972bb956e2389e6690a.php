<?php

/* AirBlogBundle:Posts:tag.html.twig */
class __TwigTemplate_a143b1c5d1efb0b355db0a090d51a000a4bdaf8d3b7d8972bb956e2389e6690a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        try {
            $this->parent = $this->env->loadTemplate("AirBlogBundle::base.html.twig");
        } catch (Twig_Error_Loader $e) {
            $e->setTemplateFile($this->getTemplateName());
            $e->setTemplateLine(1);

            throw $e;
        }

        $this->blocks = array(
            'pageTitle' => array($this, 'block_pageTitle'),
            'contentTitle' => array($this, 'block_contentTitle'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "AirBlogBundle::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_pageTitle($context, array $blocks = array())
    {
        echo "Wpisy z tagiem \"Tag\"";
    }

    // line 5
    public function block_contentTitle($context, array $blocks = array())
    {
        echo "Wpisy z tagiem \"Tag\"";
    }

    // line 7
    public function block_content($context, array $blocks = array())
    {
        // line 8
        echo "<div class=\"left-side\">
                        
    <article class=\"post\">
        <header>
            <h1>Suspendisse molestie vivamus fermentum</h1>

            <div class=\"meta-data\">
                <div class=\"author\">Autor <a href=\"#\">macq</a></div>
                <div class=\"create-date\">Dodano 11.12.2013</div>
            </div>
        </header>

        <div class=\"thumbnail\">
            <div class=\"meta-data\"> 
                <div class=\"categories\">
                    Kategoria:
                    <a href=\"#\">Samoloty</a>
                </div>

                <div class=\"tags\">
                    Tagi:
                    <a href=\"#\">A380</a>
                    <a href=\"#\">airbus</a>
                    <a href=\"#\">odrzutowe</a> 
                </div>
            </div>

            <img src=\"img/default-thumbnail.jpg\" alt=\"\">
        </div>

        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Reiciendis, omnis asperiores maiores consequatur eligendi vitae veniam ab similique nulla ut necessitatibus error rem dolore architecto culpa magni aliquid ea! Consequuntur, harum, eum, laboriosam, sunt perspiciatis accusantium totam ea nostrum magni quia molestias odit ad repudiandae necessitatibus nemo autem vero. Quas, laudantium, ipsam, dicta, nihil amet ab veniam veritatis fugit ex repudiandae minima rerum illo! Harum nihil aliquam officia autem quam!</p>

        <footer>
            <a class=\"comments\" href=\"#\">Komentarzy (31)</a>
            <a href=\"#\" class=\"btn-green\">Czytaj całość [...]</a>
        </footer>
    </article> <!-- .post -->

    <article class=\"post\">
        <header>
            <h1>Donec sit amet eleifend purus laoreet</h1>

            <div class=\"meta-data\">
                <div class=\"author\">Autor <a href=\"#\">macq</a></div>
                <div class=\"create-date\">Dodano 11.12.2013</div>
            </div>
        </header>

        <div class=\"thumbnail\">
            <div class=\"meta-data\">
                <div class=\"categories\">
                    Kategoria: 
                    <a href=\"#\">Samoloty</a>
                </div>

                <div class=\"tags\">
                    Tagi: 
                    <a href=\"#\">#A380</a>
                    <a href=\"#\">airbus</a>
                    <a href=\"#\">odrzutowe</a>
                </div>
            </div>

            <img src=\"img/default-thumbnail.jpg\" alt=\"\">
        </div>

        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Reiciendis, omnis asperiores maiores consequatur eligendi vitae veniam ab similique nulla ut necessitatibus error rem dolore architecto culpa magni aliquid ea! Consequuntur, harum, eum, laboriosam, sunt perspiciatis accusantium totam ea nostrum magni quia molestias odit ad repudiandae necessitatibus nemo autem vero. Quas, laudantium, ipsam, dicta, nihil amet ab veniam veritatis fugit ex repudiandae minima rerum illo! Harum nihil aliquam officia autem quam!</p>

        <footer>
            <a class=\"comments\" href=\"#\">Komentarzy (31)</a>
            <a href=\"#\" class=\"btn-green\">Czytaj całość [...]</a>
        </footer>
    </article> <!-- .post -->

    <div class=\"pagination\">
        <ul>
            <li><a href=\"#\">1</a></li>
            <li><a href=\"#\">2</a></li>
            <li><a class=\"current\" href=\"#\">3</a></li>
            <li><a href=\"#\">4</a></li>
        </ul>
    </div> <!-- .pagination -->
</div> <!-- .left-side -->
";
    }

    public function getTemplateName()
    {
        return "AirBlogBundle:Posts:tag.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  53 => 8,  50 => 7,  44 => 5,  38 => 3,  11 => 1,);
    }
}
