<?php

/* AirBlogBundle:Pagination:pagination.html.twig */
class __TwigTemplate_bcabd2e28ef8faf174f3b152ef3a05ccd80a723529a3982c00cef15482461784 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        if (((isset($context["pageCount"]) ? $context["pageCount"] : null) > 1)) {
            // line 2
            echo "<div class=\"pagination\">
    <ul>

    ";
            // line 5
            if (array_key_exists("previous", $context)) {
                // line 6
                echo "        <li>
            <a href=\"";
                // line 7
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath((isset($context["route"]) ? $context["route"] : null), twig_array_merge((isset($context["query"]) ? $context["query"] : null), array((isset($context["pageParameterName"]) ? $context["pageParameterName"] : null) => (isset($context["previous"]) ? $context["previous"] : null)))), "html", null, true);
                echo "\">&laquo;&nbsp;</a>
        </li>
    ";
            } else {
                // line 10
                echo "        <li class=\"disabled\">
            <a>&laquo;&nbsp;</a>
        </li>
    ";
            }
            // line 14
            echo "
    ";
            // line 15
            if (((isset($context["startPage"]) ? $context["startPage"] : null) > 1)) {
                // line 16
                echo "        <li>
            <a href=\"";
                // line 17
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath((isset($context["route"]) ? $context["route"] : null), twig_array_merge((isset($context["query"]) ? $context["query"] : null), array((isset($context["pageParameterName"]) ? $context["pageParameterName"] : null) => 1))), "html", null, true);
                echo "\">1</a>
        </li>
        ";
                // line 19
                if (((isset($context["startPage"]) ? $context["startPage"] : null) == 3)) {
                    // line 20
                    echo "            <li>
                <a href=\"";
                    // line 21
                    echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath((isset($context["route"]) ? $context["route"] : null), twig_array_merge((isset($context["query"]) ? $context["query"] : null), array((isset($context["pageParameterName"]) ? $context["pageParameterName"] : null) => 2))), "html", null, true);
                    echo "\">2</a>
            </li>
        ";
                } elseif (((isset($context["startPage"]) ? $context["startPage"] : null) != 2)) {
                    // line 24
                    echo "        <li class=\"disabled\">
            <a>&hellip;</a>
        </li>
        ";
                }
                // line 28
                echo "    ";
            }
            // line 29
            echo "
    ";
            // line 30
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["pagesInRange"]) ? $context["pagesInRange"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["page"]) {
                // line 31
                echo "        ";
                if (($context["page"] != (isset($context["current"]) ? $context["current"] : null))) {
                    // line 32
                    echo "            <li>
                <a href=\"";
                    // line 33
                    echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath((isset($context["route"]) ? $context["route"] : null), twig_array_merge((isset($context["query"]) ? $context["query"] : null), array((isset($context["pageParameterName"]) ? $context["pageParameterName"] : null) => $context["page"]))), "html", null, true);
                    echo "\">";
                    echo twig_escape_filter($this->env, $context["page"], "html", null, true);
                    echo "</a>
            </li>
        ";
                } else {
                    // line 36
                    echo "            <li>
                <a class=\"current\">";
                    // line 37
                    echo twig_escape_filter($this->env, $context["page"], "html", null, true);
                    echo "</a>
            </li>
        ";
                }
                // line 40
                echo "
    ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['page'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 42
            echo "
    ";
            // line 43
            if (((isset($context["pageCount"]) ? $context["pageCount"] : null) > (isset($context["endPage"]) ? $context["endPage"] : null))) {
                // line 44
                echo "        ";
                if (((isset($context["pageCount"]) ? $context["pageCount"] : null) > ((isset($context["endPage"]) ? $context["endPage"] : null) + 1))) {
                    // line 45
                    echo "            ";
                    if (((isset($context["pageCount"]) ? $context["pageCount"] : null) > ((isset($context["endPage"]) ? $context["endPage"] : null) + 2))) {
                        // line 46
                        echo "                <li class=\"disabled\">
                    <a>&hellip;</a>
                </li>
            ";
                    } else {
                        // line 50
                        echo "                <li>
                    <a href=\"";
                        // line 51
                        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath((isset($context["route"]) ? $context["route"] : null), twig_array_merge((isset($context["query"]) ? $context["query"] : null), array((isset($context["pageParameterName"]) ? $context["pageParameterName"] : null) => ((isset($context["pageCount"]) ? $context["pageCount"] : null) - 1)))), "html", null, true);
                        echo "\">";
                        echo twig_escape_filter($this->env, ((isset($context["pageCount"]) ? $context["pageCount"] : null) - 1), "html", null, true);
                        echo "</a>
                </li>
            ";
                    }
                    // line 54
                    echo "        ";
                }
                // line 55
                echo "        <li>
            <a href=\"";
                // line 56
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath((isset($context["route"]) ? $context["route"] : null), twig_array_merge((isset($context["query"]) ? $context["query"] : null), array((isset($context["pageParameterName"]) ? $context["pageParameterName"] : null) => (isset($context["pageCount"]) ? $context["pageCount"] : null)))), "html", null, true);
                echo "\">";
                echo twig_escape_filter($this->env, (isset($context["pageCount"]) ? $context["pageCount"] : null), "html", null, true);
                echo "</a>
        </li>
    ";
            }
            // line 59
            echo "
    ";
            // line 60
            if (array_key_exists("next", $context)) {
                // line 61
                echo "        <li>
            <a href=\"";
                // line 62
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath((isset($context["route"]) ? $context["route"] : null), twig_array_merge((isset($context["query"]) ? $context["query"] : null), array((isset($context["pageParameterName"]) ? $context["pageParameterName"] : null) => (isset($context["next"]) ? $context["next"] : null)))), "html", null, true);
                echo "\">&nbsp;&raquo;</a>
        </li>
    ";
            } else {
                // line 65
                echo "        <li class=\"disabled\">
            <a>&nbsp;&raquo;</a>
        </li>
    ";
            }
            // line 69
            echo "    </ul>
</div>
";
        }
    }

    public function getTemplateName()
    {
        return "AirBlogBundle:Pagination:pagination.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  175 => 69,  169 => 65,  163 => 62,  160 => 61,  158 => 60,  155 => 59,  147 => 56,  144 => 55,  141 => 54,  133 => 51,  130 => 50,  124 => 46,  121 => 45,  118 => 44,  116 => 43,  113 => 42,  106 => 40,  100 => 37,  97 => 36,  89 => 33,  86 => 32,  83 => 31,  79 => 30,  76 => 29,  73 => 28,  67 => 24,  61 => 21,  58 => 20,  56 => 19,  51 => 17,  48 => 16,  46 => 15,  43 => 14,  37 => 10,  31 => 7,  28 => 6,  26 => 5,  21 => 2,  19 => 1,);
    }
}
