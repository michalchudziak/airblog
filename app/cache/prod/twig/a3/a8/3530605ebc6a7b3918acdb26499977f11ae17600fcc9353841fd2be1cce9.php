<?php

/* AirAdminBundle:Categories:index.html.twig */
class __TwigTemplate_a3a83530605ebc6a7b3918acdb26499977f11ae17600fcc9353841fd2be1cce9 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        try {
            $this->parent = $this->env->loadTemplate("AirAdminBundle::base.html.twig");
        } catch (Twig_Error_Loader $e) {
            $e->setTemplateFile($this->getTemplateName());
            $e->setTemplateLine(1);

            throw $e;
        }

        $this->blocks = array(
            'pageTitle' => array($this, 'block_pageTitle'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "AirAdminBundle::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_pageTitle($context, array $blocks = array())
    {
        // line 4
        echo "\t";
        $this->displayParentBlock("pageTitle", $context, $blocks);
        echo " - Kategorie
";
    }

    // line 7
    public function block_content($context, array $blocks = array())
    {
        // line 8
        echo "<div class=\"container\">
    <div class=\"row\">
        <div class=\"col-md-12\">

            <div class=\"panel panel-default\">
                <div class=\"panel-heading\">Kategorie</div>
                <div class=\"panel-body\">

                    <ul class=\"nav nav-pills\">
                        <li><a href=\"";
        // line 17
        echo $this->env->getExtension('routing')->getPath("admin_categoryForm");
        echo "\">Dodaj nową</a></li>
                    </ul>

                    <div class=\"clearfix\"></div>

                    <div style=\"margin-top: 20px;\" class=\"table-responsive\">
                        <table class=\"table table-striped table-bordered table-hover\">
                            <thead>
                                <tr>
                                    <th>";
        // line 26
        echo $this->env->getExtension('knp_pagination')->sortable((isset($context["pagination"]) ? $context["pagination"] : null), "ID", "t.id");
        echo "</th>
                                    <th>";
        // line 27
        echo $this->env->getExtension('knp_pagination')->sortable((isset($context["pagination"]) ? $context["pagination"] : null), "Nazwa", "p.name");
        echo "</th>
                                    <th>";
        // line 28
        echo $this->env->getExtension('knp_pagination')->sortable((isset($context["pagination"]) ? $context["pagination"] : null), "Ilość postów przypisanych", "postsCount");
        echo "</th>
                                    <th class=\"actions\">Akcje</th>
                                </tr>
                            </thead>
                            <tbody>
                                ";
        // line 33
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["pagination"]) ? $context["pagination"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["row"]) {
            // line 34
            echo "                                    ";
            $context["category"] = $this->getAttribute($context["row"], 0, array(), "array");
            // line 35
            echo "                                    ";
            $context["editPath"] = $this->env->getExtension('routing')->getPath("admin_categoryForm", array("id" => $this->getAttribute((isset($context["category"]) ? $context["category"] : null), "id", array())));
            // line 36
            echo "                                    <tr>
                                        <td>";
            // line 37
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["category"]) ? $context["category"] : null), "id", array()), "html", null, true);
            echo "</td>
                                        <td><a href=\"";
            // line 38
            echo twig_escape_filter($this->env, (isset($context["editPath"]) ? $context["editPath"] : null), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["category"]) ? $context["category"] : null), "name", array()), "html", null, true);
            echo "</a></td>
                                        <td>";
            // line 39
            echo twig_escape_filter($this->env, $this->getAttribute($context["row"], "postsCount", array(), "array"), "html", null, true);
            echo "</td>
                                        <td>
                                            <div class=\"btn-group\">
                                                <a href=\"";
            // line 42
            echo twig_escape_filter($this->env, (isset($context["editPath"]) ? $context["editPath"] : null), "html", null, true);
            echo "\" class=\"btn btn-primary\"><span class=\"glyphicon glyphicon-edit\"></span></a>
                                                <a href=\"";
            // line 43
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("admin_categoryDelete", array("id" => $this->getAttribute((isset($context["category"]) ? $context["category"] : null), "id", array()))), "html", null, true);
            echo "\" class=\"btn btn-danger\"><span class=\"glyphicon glyphicon-trash\"></span></a>
                                            </div>
                                        </td>
                                    </tr>
                                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['row'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 48
        echo "                            </tbody>
                        </table>
                    </div>

                    ";
        // line 52
        echo $this->env->getExtension('knp_pagination')->render((isset($context["pagination"]) ? $context["pagination"] : null), "AirAdminBundle:Pagination:admin_pagination.html.twig");
        echo "
                </div>  <!-- .panel-body -->
            </div>  <!-- .panel -->
        </div> <!-- .col-md-12 -->
    </div> <!-- .row -->
</div> <!-- .container -->
";
    }

    public function getTemplateName()
    {
        return "AirAdminBundle:Categories:index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  139 => 52,  133 => 48,  122 => 43,  118 => 42,  112 => 39,  106 => 38,  102 => 37,  99 => 36,  96 => 35,  93 => 34,  89 => 33,  81 => 28,  77 => 27,  73 => 26,  61 => 17,  50 => 8,  47 => 7,  40 => 4,  37 => 3,  11 => 1,);
    }
}
