<?php

/* CommonUserBundle:Admin:form.html.twig */
class __TwigTemplate_10ae5aa8c3e3930d623822fb16d961d0a87a1af1b3e52dffb72e5095228643e3 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        try {
            $this->parent = $this->env->loadTemplate("AirAdminBundle::base.html.twig");
        } catch (Twig_Error_Loader $e) {
            $e->setTemplateFile($this->getTemplateName());
            $e->setTemplateLine(1);

            throw $e;
        }

        $this->blocks = array(
            'pageTitle' => array($this, 'block_pageTitle'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "AirAdminBundle::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_pageTitle($context, array $blocks = array())
    {
        // line 4
        echo "    ";
        $this->displayParentBlock("pageTitle", $context, $blocks);
        echo " - Profil użytkownika \"";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["user"]) ? $context["user"] : null), "username", array()), "html", null, true);
        echo "\"
";
    }

    // line 7
    public function block_content($context, array $blocks = array())
    {
        // line 8
        echo "<div class=\"container\">
    ";
        // line 9
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["form"]) ? $context["form"] : null), 'form_start');
        echo "
        <div class=\"row\">
            <div class=\"col-md-8\">
                <div class=\"panel panel-default\">
                    <div class=\"panel-heading\">
                        Dane podstawowe
                    </div>
                    <div class=\"panel-body\">
                        ";
        // line 17
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "username", array()), 'row');
        echo "
                        ";
        // line 18
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "email", array()), 'row');
        echo "
                        ";
        // line 19
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "roles", array()), 'row', array("attr" => array("class" => "select-block")));
        echo "
                    </div>
                </div>
            </div> <!-- .col-md-8 -->

            <div class=\"col-md-4\">
                <div class=\"row\">
                    <div class=\"col-md-12\">
                        <div class=\"panel panel-default\">
                            <div class=\"panel-heading\">
                                Akcje
                            </div>
                            <div class=\"panel-body\">
                                <button class=\"btn btn-success\">Zapisz</button>
                            </div>
                        </div>
                    </div>  <!-- .col-md-12 -->

                    <div class=\"col-md-12\">
                        <div class=\"panel panel-default\">
                            <div class=\"panel-heading\">
                                Dostepność
                            </div>
                            <div class=\"panel-body account-checkbox\">
                                ";
        // line 43
        $context["labelWidth"] = 9;
        // line 44
        echo "                                ";
        $context["wrapperWidth"] = 3;
        // line 45
        echo "                                ";
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "accountExpired", array()), 'row', array("label_length" => (isset($context["labelWidth"]) ? $context["labelWidth"] : null), "wrapper_length" => (isset($context["wrapperWidth"]) ? $context["wrapperWidth"] : null)));
        echo "
                                ";
        // line 46
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "accountLocked", array()), 'row', array("label_length" => (isset($context["labelWidth"]) ? $context["labelWidth"] : null), "wrapper_length" => (isset($context["wrapperWidth"]) ? $context["wrapperWidth"] : null)));
        echo "
                                ";
        // line 47
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "credentialsExpired", array()), 'row', array("label_length" => (isset($context["labelWidth"]) ? $context["labelWidth"] : null), "wrapper_length" => (isset($context["wrapperWidth"]) ? $context["wrapperWidth"] : null)));
        echo "
                                ";
        // line 48
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "enabled", array()), 'row', array("label_length" => (isset($context["labelWidth"]) ? $context["labelWidth"] : null), "wrapper_length" => (isset($context["wrapperWidth"]) ? $context["wrapperWidth"] : null)));
        echo "
                            </div> <!-- .panel-body -->
                        </div> <!-- .panel -->
                    </div> <!-- .col-md-12 -->
                </div> <!-- .row -->
            </div> <!-- .col-md-4 -->
        </div> <!-- .row -->
        
        ";
        // line 56
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : null), 'rest');
        echo "
    ";
        // line 57
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["form"]) ? $context["form"] : null), 'form_end');
        echo "
</div> <!-- .container -->
";
    }

    public function getTemplateName()
    {
        return "CommonUserBundle:Admin:form.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  134 => 57,  130 => 56,  119 => 48,  115 => 47,  111 => 46,  106 => 45,  103 => 44,  101 => 43,  74 => 19,  70 => 18,  66 => 17,  55 => 9,  52 => 8,  49 => 7,  40 => 4,  37 => 3,  11 => 1,);
    }
}
