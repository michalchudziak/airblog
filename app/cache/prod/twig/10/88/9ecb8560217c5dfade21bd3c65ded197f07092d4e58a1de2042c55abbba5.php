<?php

/* AirBlogBundle::base.html.twig */
class __TwigTemplate_10889ecb8560217c5dfade21bd3c65ded197f07092d4e58a1de2042c55abbba5 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'pageTitle' => array($this, 'block_pageTitle'),
            'pageDescription' => array($this, 'block_pageDescription'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'topImage' => array($this, 'block_topImage'),
            'contentTitle' => array($this, 'block_contentTitle'),
            'content' => array($this, 'block_content'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!DOCTYPE html>
<html class=\"no-js\">
    <head>
        <meta charset=\"utf-8\">
        <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">
        <title>";
        // line 6
        ob_start();
        $this->displayBlock('pageTitle', $context, $blocks);
        echo trim(preg_replace('/>\s+</', '><', ob_get_clean()));
        echo " | AirBlog</title>
        <link rel=\"icon\" type=\"image/png\" href=\"/favicon.png\">
        <meta name=\"description\" content=\"";
        // line 8
        ob_start();
        $this->displayBlock('pageDescription', $context, $blocks);
        echo trim(preg_replace('/>\s+</', '><', ob_get_clean()));
        echo "\">
        <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">

        ";
        // line 11
        $this->displayBlock('stylesheets', $context, $blocks);
        // line 24
        echo "    </head>
    <body>        
        <header id=\"header\">
            <div class=\"top\">
                <div class=\"pos-center\">
                    
                    <a class=\"mobile-menu-trigger\" href=\"#\">
                        <span class=\"bar\"></span>
                        <span class=\"bar\"></span>
                        <span class=\"bar\"></span>
                    </a>
                    
                    <div class=\"menu\">
                        <nav>
                            ";
        // line 38
        echo $this->env->getExtension('air_blog_extension')->printMainMenu();
        echo "
                        </nav>
                        
                        ";
        // line 41
        echo $this->env->getExtension('air_blog_extension')->printCategoriesList();
        echo "
                        
                        ";
        // line 43
        if ($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array())) {
            // line 44
            echo "                        <div class=\"user-panel\">
                            <span class=\"user-name\">Zalogowany jako <a href=\"";
            // line 45
            echo $this->env->getExtension('routing')->getPath("user_accountSettings");
            echo "\">";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "username", array()), "html", null, true);
            echo "</a></span>
                            <a class=\"btn-green\" href=\"";
            // line 46
            echo $this->env->getExtension('routing')->getPath("_logout");
            echo "\">Wyloguj</a>
                        </div>
                        ";
        } else {
            // line 49
            echo "                        <div class=\"user-panel\">
                            <div class=\"btn-group\">
                                ";
            // line 51
            $context["loginUrl"] = $this->env->getExtension('routing')->getPath("blog_login");
            // line 52
            echo "                                <a class=\"btn-green\" href=\"";
            echo twig_escape_filter($this->env, (isset($context["loginUrl"]) ? $context["loginUrl"] : null), "html", null, true);
            echo "\">Zaloguj</a>
                                <a class=\"btn-green\" href=\"";
            // line 53
            echo twig_escape_filter($this->env, (isset($context["loginUrl"]) ? $context["loginUrl"] : null), "html", null, true);
            echo "\">Zarejestruj</a>
                            </div>
                        </div> 
                        ";
        }
        // line 57
        echo "                       
                    </div>
                    <div class=\"clearfix\"></div>
                </div>
            </div>

            ";
        // line 63
        $this->displayBlock('topImage', $context, $blocks);
        // line 66
        echo "        </header>

        <div id=\"main\">
            <div class=\"header\">
                <div class=\"pos-center\">
                    <h2 class=\"left-side\">";
        // line 71
        ob_start();
        $this->displayBlock('contentTitle', $context, $blocks);
        echo trim(preg_replace('/>\s+</', '><', ob_get_clean()));
        echo "</h2>
                    ";
        // line 72
        if ( !array_key_exists("hideSearch", $context)) {
            // line 73
            echo "                    <form class=\"right-side\" action=\"";
            echo $this->env->getExtension('routing')->getPath("blog_search");
            echo "\">
                        <input type=\"text\" name=\"search\" value=\"";
            // line 74
            echo twig_escape_filter($this->env, ((array_key_exists("searchParam", $context)) ? ((isset($context["searchParam"]) ? $context["searchParam"] : null)) : ("")), "html", null, true);
            echo "\" placeholder=\"Wyszukaj\">
                    </form>
                    ";
        }
        // line 77
        echo "                </div>
            </div>

            <div class=\"content\">
                <div class=\"pos-center\">
                    ";
        // line 82
        $this->displayBlock('content', $context, $blocks);
        // line 83
        echo "                    ";
        if ( !array_key_exists("hideSidebar", $context)) {
            // line 84
            echo "                    <div class=\"right-side sidebar\">
                        <div class=\"menu\">
                            <h3>Kategorie</h3>
                            ";
            // line 87
            echo $this->env->getExtension('air_blog_extension')->printCategoriesList();
            echo "
                        </div>
                        ";
            // line 89
            echo $this->env->getExtension('air_blog_extension')->tagsCloud();
            echo "
                        ";
            // line 90
            echo $this->env->getExtension('air_blog_extension')->recentCommented();
            echo "

                    </div> <!-- .right-side -->
                    ";
        }
        // line 94
        echo "                </div> <!-- .content -->

                <div class=\"clearfix\"></div>
            </div> <!-- .pos-center -->
        </div> <!-- #main -->

        <footer id=\"footer\">
            <div class=\"top\">
                <div class=\"pos-center\">
                    ";
        // line 103
        echo $this->env->getExtension('air_blog_extension')->printMainMenu();
        echo "
                </div>
            </div>

            <div class=\"bottom\">
                <div class=\"pos-center\">
                    <div class=\"copyrights\">
                        <p>&copy; 2014 All rights reserved --Na potrzeby kursu Programowanie Systemów Webowych--.</p>
                        <span>Autorzy: Michał Chudziak, Piotr Drapich</span>
                    </div>
                </div>
            </div>
        </footer>

        ";
        // line 117
        $this->displayBlock('javascripts', $context, $blocks);
        // line 129
        echo "    </body>
</html>
";
    }

    // line 6
    public function block_pageTitle($context, array $blocks = array())
    {
    }

    // line 8
    public function block_pageDescription($context, array $blocks = array())
    {
    }

    // line 11
    public function block_stylesheets($context, array $blocks = array())
    {
        // line 12
        echo "        <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700&subset=latin-ext,latin' rel='stylesheet' type='text/css'>
        
            ";
        // line 14
        if (isset($context['assetic']['debug']) && $context['assetic']['debug']) {
            // asset "63fad83_0"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_63fad83_0") : $this->env->getExtension('assets')->getAssetUrl("assets/css/compiled_normalize_1.css");
            // line 20
            echo "                <link rel=\"stylesheet\" href=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\">
            ";
            // asset "63fad83_1"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_63fad83_1") : $this->env->getExtension('assets')->getAssetUrl("assets/css/compiled_main_2.css");
            echo "                <link rel=\"stylesheet\" href=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\">
            ";
        } else {
            // asset "63fad83"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_63fad83") : $this->env->getExtension('assets')->getAssetUrl("assets/css/compiled.css");
            echo "                <link rel=\"stylesheet\" href=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\">
            ";
        }
        unset($context["asset_url"]);
        // line 22
        echo "
        ";
    }

    // line 63
    public function block_topImage($context, array $blocks = array())
    {
        // line 64
        echo "            <div class=\"top-image\"></div>
            ";
    }

    // line 71
    public function block_contentTitle($context, array $blocks = array())
    {
    }

    // line 82
    public function block_content($context, array $blocks = array())
    {
    }

    // line 117
    public function block_javascripts($context, array $blocks = array())
    {
        // line 118
        echo "            ";
        if (isset($context['assetic']['debug']) && $context['assetic']['debug']) {
            // asset "6aec9ce_0"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_6aec9ce_0") : $this->env->getExtension('assets')->getAssetUrl("assets/js/compiled_jquery-1.10.2.min_1.js");
            // line 126
            echo "                <script src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\"></script>
            ";
            // asset "6aec9ce_1"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_6aec9ce_1") : $this->env->getExtension('assets')->getAssetUrl("assets/js/compiled_part_2_jquery.responsiSlider_2.js");
            echo "                <script src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\"></script>
            ";
            // asset "6aec9ce_2"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_6aec9ce_2") : $this->env->getExtension('assets')->getAssetUrl("assets/js/compiled_part_2_modernizr-2.7.0.min_3.js");
            echo "                <script src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\"></script>
            ";
            // asset "6aec9ce_3"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_6aec9ce_3") : $this->env->getExtension('assets')->getAssetUrl("assets/js/compiled_plugins_3.js");
            echo "                <script src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\"></script>
            ";
            // asset "6aec9ce_4"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_6aec9ce_4") : $this->env->getExtension('assets')->getAssetUrl("assets/js/compiled_main_4.js");
            echo "                <script src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\"></script>
            ";
        } else {
            // asset "6aec9ce"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_6aec9ce") : $this->env->getExtension('assets')->getAssetUrl("assets/js/compiled.js");
            echo "                <script src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\"></script>
            ";
        }
        unset($context["asset_url"]);
        // line 128
        echo "        ";
    }

    public function getTemplateName()
    {
        return "AirBlogBundle::base.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  327 => 128,  289 => 126,  284 => 118,  281 => 117,  276 => 82,  271 => 71,  266 => 64,  263 => 63,  258 => 22,  238 => 20,  234 => 14,  230 => 12,  227 => 11,  222 => 8,  217 => 6,  211 => 129,  209 => 117,  192 => 103,  181 => 94,  174 => 90,  170 => 89,  165 => 87,  160 => 84,  157 => 83,  155 => 82,  148 => 77,  142 => 74,  137 => 73,  135 => 72,  129 => 71,  122 => 66,  120 => 63,  112 => 57,  105 => 53,  100 => 52,  98 => 51,  94 => 49,  88 => 46,  82 => 45,  79 => 44,  77 => 43,  72 => 41,  66 => 38,  50 => 24,  48 => 11,  40 => 8,  33 => 6,  26 => 1,);
    }
}
