<?php

/* AirAdminBundle:Pagination:sortable_link.html.twig */
class __TwigTemplate_22ca55ea2cc74463798e8e6fcdd9757e9cce7063bd6c5aeebcb2ce24011d238d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        if (($this->getAttribute((isset($context["options"]) ? $context["options"] : null), "class", array(), "array", true, true) && !twig_in_filter("sortable", $this->getAttribute((isset($context["options"]) ? $context["options"] : null), "class", array(), "array")))) {
            // line 2
            echo "    ";
            $context["options"] = twig_array_merge((isset($context["options"]) ? $context["options"] : null), array("class" => ($this->getAttribute((isset($context["options"]) ? $context["options"] : null), "class", array(), "array") . " sortable")));
        }
        // line 4
        echo "
<a";
        // line 5
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["options"]) ? $context["options"] : null));
        foreach ($context['_seq'] as $context["attr"] => $context["value"]) {
            echo " ";
            echo twig_escape_filter($this->env, $context["attr"], "html", null, true);
            echo "=\"";
            echo twig_escape_filter($this->env, $context["value"], "html", null, true);
            echo "\"";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['attr'], $context['value'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        echo ">";
        echo twig_escape_filter($this->env, (isset($context["title"]) ? $context["title"] : null), "html", null, true);
        echo "</a>
";
    }

    public function getTemplateName()
    {
        return "AirAdminBundle:Pagination:sortable_link.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  28 => 5,  25 => 4,  21 => 2,  19 => 1,);
    }
}
