<?php

/* AirAdminBundle:Form:form_template.html.twig */
class __TwigTemplate_c021a4a9fa212ab02e880fc3362aa5d325af1a70ee44efdbab7d37f3e6e20972 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'form_start' => array($this, 'block_form_start'),
            'form_row' => array($this, 'block_form_row'),
            'checkbox_row' => array($this, 'block_checkbox_row'),
            'form_errors' => array($this, 'block_form_errors'),
            'form_label' => array($this, 'block_form_label'),
            'form_widget_simple' => array($this, 'block_form_widget_simple'),
            'submit_widget' => array($this, 'block_submit_widget'),
            'ckeditor_row' => array($this, 'block_ckeditor_row'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        $this->displayBlock('form_start', $context, $blocks);
        // line 18
        echo "

";
        // line 20
        $this->displayBlock('form_row', $context, $blocks);
        // line 31
        echo "
";
        // line 32
        $this->displayBlock('checkbox_row', $context, $blocks);
        // line 39
        echo "

";
        // line 41
        $this->displayBlock('form_errors', $context, $blocks);
        // line 50
        echo "    

";
        // line 52
        $this->displayBlock('form_label', $context, $blocks);
        // line 73
        echo "        

";
        // line 75
        $this->displayBlock('form_widget_simple', $context, $blocks);
        // line 84
        echo "

";
        // line 86
        $this->displayBlock('submit_widget', $context, $blocks);
        // line 93
        echo "
";
        // line 94
        $this->displayBlock('ckeditor_row', $context, $blocks);
    }

    // line 1
    public function block_form_start($context, array $blocks = array())
    {
        // line 2
        ob_start();
        // line 3
        echo "    ";
        $context["method"] = twig_upper_filter($this->env, (isset($context["method"]) ? $context["method"] : null));
        // line 4
        echo "    ";
        if (twig_in_filter((isset($context["method"]) ? $context["method"] : null), array(0 => "GET", 1 => "POST"))) {
            // line 5
            echo "        ";
            $context["form_method"] = (isset($context["method"]) ? $context["method"] : null);
            // line 6
            echo "    ";
        } else {
            // line 7
            echo "        ";
            $context["form_method"] = "POST";
            // line 8
            echo "    ";
        }
        echo "    
    ";
        // line 9
        if ( !$this->getAttribute((isset($context["attr"]) ? $context["attr"] : null), "class", array(), "array", true, true)) {
            // line 10
            echo "        ";
            $context["attr"] = twig_array_merge((isset($context["attr"]) ? $context["attr"] : null), array("class" => "form-horizontal"));
            // line 11
            echo "    ";
        }
        // line 12
        echo "    <form name=\"";
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "vars", array()), "name", array()), "html", null, true);
        echo "\" method=\"";
        echo twig_escape_filter($this->env, twig_lower_filter($this->env, (isset($context["form_method"]) ? $context["form_method"] : null)), "html", null, true);
        echo "\" action=\"";
        echo twig_escape_filter($this->env, (isset($context["action"]) ? $context["action"] : null), "html", null, true);
        echo "\"";
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["attr"]) ? $context["attr"] : null));
        foreach ($context['_seq'] as $context["attrname"] => $context["attrvalue"]) {
            echo " ";
            echo twig_escape_filter($this->env, $context["attrname"], "html", null, true);
            echo "=\"";
            echo twig_escape_filter($this->env, $context["attrvalue"], "html", null, true);
            echo "\"";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['attrname'], $context['attrvalue'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        if ((isset($context["multipart"]) ? $context["multipart"] : null)) {
            echo " enctype=\"multipart/form-data\"";
        }
        echo " novalidate=\"true\">
    ";
        // line 13
        if (((isset($context["form_method"]) ? $context["form_method"] : null) != (isset($context["method"]) ? $context["method"] : null))) {
            // line 14
            echo "        <input type=\"hidden\" name=\"_method\" value=\"";
            echo twig_escape_filter($this->env, (isset($context["method"]) ? $context["method"] : null), "html", null, true);
            echo "\" />
    ";
        }
        echo trim(preg_replace('/>\s+</', '><', ob_get_clean()));
    }

    // line 20
    public function block_form_row($context, array $blocks = array())
    {
        // line 21
        ob_start();
        // line 22
        echo "    <div class=\"form-group";
        if ((twig_length_filter($this->env, (isset($context["errors"]) ? $context["errors"] : null)) > 0)) {
            echo " has-error";
        }
        echo "\">
        ";
        // line 23
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : null), 'label');
        echo "
        <div class=\"";
        // line 24
        echo twig_escape_filter($this->env, sprintf("col-lg-%d", ((array_key_exists("wrapper_length", $context)) ? (_twig_default_filter((isset($context["wrapper_length"]) ? $context["wrapper_length"] : null), 10)) : (10))), "html", null, true);
        echo "\">
            ";
        // line 25
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : null), 'widget');
        echo "
            ";
        // line 26
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : null), 'errors');
        echo "
        </div>
    </div>
";
        echo trim(preg_replace('/>\s+</', '><', ob_get_clean()));
    }

    // line 32
    public function block_checkbox_row($context, array $blocks = array())
    {
        // line 33
        ob_start();
        // line 34
        echo "    <div class=\"checkbox\">
        <label>";
        // line 35
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : null), 'widget');
        echo " ";
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "vars", array()), "label", array()), "html", null, true);
        echo "</label>
    </div>
";
        echo trim(preg_replace('/>\s+</', '><', ob_get_clean()));
    }

    // line 41
    public function block_form_errors($context, array $blocks = array())
    {
        // line 42
        ob_start();
        // line 43
        echo "    ";
        if ((twig_length_filter($this->env, (isset($context["errors"]) ? $context["errors"] : null)) > 0)) {
            // line 44
            echo "        ";
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["errors"]) ? $context["errors"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["error"]) {
                // line 45
                echo "            <p class=\"help-block error\">";
                echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans($this->getAttribute($context["error"], "message", array())), "html", null, true);
                echo "</p>
        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['error'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 47
            echo "    ";
        }
        echo trim(preg_replace('/>\s+</', '><', ob_get_clean()));
    }

    // line 52
    public function block_form_label($context, array $blocks = array())
    {
        // line 53
        ob_start();
        // line 54
        echo "    ";
        if ( !((isset($context["label"]) ? $context["label"] : null) === false)) {
            // line 55
            echo "        ";
            if ( !array_key_exists("label_length", $context)) {
                // line 56
                echo "            ";
                $context["label_length"] = 2;
                // line 57
                echo "        ";
            }
            // line 58
            echo "        ";
            $context["defCssClass"] = sprintf(" col-lg-%d control-label", (isset($context["label_length"]) ? $context["label_length"] : null));
            // line 59
            echo "        ";
            $context["label_attr"] = twig_array_merge((isset($context["label_attr"]) ? $context["label_attr"] : null), array("class" => trim(((($this->getAttribute((isset($context["label_attr"]) ? $context["label_attr"] : null), "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute((isset($context["label_attr"]) ? $context["label_attr"] : null), "class", array()), "")) : ("")) . (isset($context["defCssClass"]) ? $context["defCssClass"] : null)))));
            // line 60
            echo "        ";
            if ( !(isset($context["compound"]) ? $context["compound"] : null)) {
                // line 61
                echo "            ";
                $context["label_attr"] = twig_array_merge((isset($context["label_attr"]) ? $context["label_attr"] : null), array("for" => (isset($context["id"]) ? $context["id"] : null)));
                // line 62
                echo "        ";
            }
            // line 63
            echo "        ";
            if ((isset($context["required"]) ? $context["required"] : null)) {
                // line 64
                echo "            ";
                $context["label_attr"] = twig_array_merge((isset($context["label_attr"]) ? $context["label_attr"] : null), array("class" => trim(((($this->getAttribute((isset($context["label_attr"]) ? $context["label_attr"] : null), "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute((isset($context["label_attr"]) ? $context["label_attr"] : null), "class", array()), "")) : ("")) . " required"))));
                // line 65
                echo "        ";
            }
            // line 66
            echo "        ";
            if (twig_test_empty((isset($context["label"]) ? $context["label"] : null))) {
                // line 67
                echo "            ";
                $context["label"] = $this->env->getExtension('form')->humanize((isset($context["name"]) ? $context["name"] : null));
                // line 68
                echo "        ";
            }
            // line 69
            echo "        <label";
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["label_attr"]) ? $context["label_attr"] : null));
            foreach ($context['_seq'] as $context["attrname"] => $context["attrvalue"]) {
                echo " ";
                echo twig_escape_filter($this->env, $context["attrname"], "html", null, true);
                echo "=\"";
                echo twig_escape_filter($this->env, $context["attrvalue"], "html", null, true);
                echo "\"";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['attrname'], $context['attrvalue'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            echo ">";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans((isset($context["label"]) ? $context["label"] : null), array(), (isset($context["translation_domain"]) ? $context["translation_domain"] : null)), "html", null, true);
            echo "</label>
    ";
        }
        echo trim(preg_replace('/>\s+</', '><', ob_get_clean()));
    }

    // line 75
    public function block_form_widget_simple($context, array $blocks = array())
    {
        // line 76
        ob_start();
        // line 77
        echo "    ";
        $context["type"] = ((array_key_exists("type", $context)) ? (_twig_default_filter((isset($context["type"]) ? $context["type"] : null), "text")) : ("text"));
        // line 78
        echo "    ";
        if (((isset($context["type"]) ? $context["type"] : null) != "file")) {
            // line 79
            echo "        ";
            $context["attr"] = twig_array_merge((isset($context["attr"]) ? $context["attr"] : null), array("class" => trim((($this->getAttribute((isset($context["attr"]) ? $context["attr"] : null), "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute((isset($context["attr"]) ? $context["attr"] : null), "class", array()), "form-control")) : ("form-control")))));
            // line 80
            echo "    ";
        }
        // line 81
        echo "    <input type=\"";
        echo twig_escape_filter($this->env, (isset($context["type"]) ? $context["type"] : null), "html", null, true);
        echo "\" ";
        $this->displayBlock("widget_attributes", $context, $blocks);
        echo " ";
        if ( !twig_test_empty((isset($context["value"]) ? $context["value"] : null))) {
            echo "value=\"";
            echo twig_escape_filter($this->env, (isset($context["value"]) ? $context["value"] : null), "html", null, true);
            echo "\" ";
        }
        echo "/>
";
        echo trim(preg_replace('/>\s+</', '><', ob_get_clean()));
    }

    // line 86
    public function block_submit_widget($context, array $blocks = array())
    {
        // line 87
        ob_start();
        // line 88
        echo "    ";
        $context["type"] = ((array_key_exists("type", $context)) ? (_twig_default_filter((isset($context["type"]) ? $context["type"] : null), "submit")) : ("submit"));
        // line 89
        echo "    ";
        $context["attr"] = twig_array_merge((isset($context["attr"]) ? $context["attr"] : null), array("class" => trim(((($this->getAttribute((isset($context["attr"]) ? $context["attr"] : null), "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute((isset($context["attr"]) ? $context["attr"] : null), "class", array()), "")) : ("")) . " btn btn-success"))));
        // line 90
        echo "    ";
        $this->displayBlock("button_widget", $context, $blocks);
        echo "
";
        echo trim(preg_replace('/>\s+</', '><', ob_get_clean()));
    }

    // line 94
    public function block_ckeditor_row($context, array $blocks = array())
    {
        // line 95
        ob_start();
        // line 96
        echo "    <div class=\"form-group";
        if ((twig_length_filter($this->env, (isset($context["errors"]) ? $context["errors"] : null)) > 0)) {
            echo " has-error";
        }
        echo "\">
        <div class=\"col-lg-12\">
            ";
        // line 98
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : null), 'widget');
        echo "
            ";
        // line 99
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : null), 'errors');
        echo "
        </div>
    </div>
";
        echo trim(preg_replace('/>\s+</', '><', ob_get_clean()));
    }

    public function getTemplateName()
    {
        return "AirAdminBundle:Form:form_template.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  360 => 99,  356 => 98,  348 => 96,  346 => 95,  343 => 94,  335 => 90,  332 => 89,  329 => 88,  327 => 87,  324 => 86,  308 => 81,  305 => 80,  302 => 79,  299 => 78,  296 => 77,  294 => 76,  291 => 75,  269 => 69,  266 => 68,  263 => 67,  260 => 66,  257 => 65,  254 => 64,  251 => 63,  248 => 62,  245 => 61,  242 => 60,  239 => 59,  236 => 58,  233 => 57,  230 => 56,  227 => 55,  224 => 54,  222 => 53,  219 => 52,  213 => 47,  204 => 45,  199 => 44,  196 => 43,  194 => 42,  191 => 41,  181 => 35,  178 => 34,  176 => 33,  173 => 32,  164 => 26,  160 => 25,  156 => 24,  152 => 23,  145 => 22,  143 => 21,  140 => 20,  131 => 14,  129 => 13,  104 => 12,  101 => 11,  98 => 10,  96 => 9,  91 => 8,  88 => 7,  85 => 6,  82 => 5,  79 => 4,  76 => 3,  74 => 2,  71 => 1,  67 => 94,  64 => 93,  62 => 86,  58 => 84,  56 => 75,  52 => 73,  50 => 52,  46 => 50,  44 => 41,  40 => 39,  38 => 32,  35 => 31,  33 => 20,  29 => 18,  27 => 1,);
    }
}
