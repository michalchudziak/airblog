<?php

/* AirAdminBundle::base.html.twig */
class __TwigTemplate_e1bf58f6e3967ce5da4b6f7f48b9094ac7da84e46bbacce54692e0617deca9b8 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'pageTitle' => array($this, 'block_pageTitle'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'javascripts' => array($this, 'block_javascripts'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!DOCTYPE html>
<html lang=\"pl\">
    <head>
        <meta charset=\"utf-8\">
        <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">
        <meta name=\"description\" content=\"Panel Administracyjny Blog SF2.\">
        <meta name=\"author\" content=\"Michal Chudziak, Piotr Drapich\">

        <title>";
        // line 9
        ob_start();
        $this->displayBlock('pageTitle', $context, $blocks);
        echo trim(preg_replace('/>\s+</', '><', ob_get_clean()));
        echo "</title>

        <link href=\"";
        // line 11
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/airadmin/img/favicon.png"), "html", null, true);
        echo "\" rel=\"shortcut icon\">
    
        ";
        // line 13
        $this->displayBlock('stylesheets', $context, $blocks);
        // line 24
        echo "
        ";
        // line 25
        $this->displayBlock('javascripts', $context, $blocks);
        // line 35
        echo "
    </head>

    <body>
        <nav class=\"navbar navbar-inverse\" role=\"navigation\">
            <div class=\"navbar-header\">
                <button type=\"button\" class=\"navbar-toggle\" data-toggle=\"collapse\" data-target=\".navbar-ex1-collapse\">
                    <span class=\"sr-only\">Rozwiń menu</span>
                    <span class=\"icon-bar\"></span>
                    <span class=\"icon-bar\"></span>
                    <span class=\"icon-bar\"></span>
                </button>
                
                <a class=\"navbar-brand\" href=\"";
        // line 48
        echo $this->env->getExtension('routing')->getPath("blog_index");
        echo "\">Blog</a>
            </div>

            <div class=\"collapse navbar-collapse navbar-ex1-collapse\">
                
                <ul class=\"nav navbar-nav\">
                    ";
        // line 54
        if ( !array_key_exists("currPage", $context)) {
            $context["currPage"] = "dashboard";
        }
        // line 55
        echo "                        
                    <li";
        // line 56
        if (((isset($context["currPage"]) ? $context["currPage"] : null) == "dashboard")) {
            echo " class=\"active\"";
        }
        echo "><a href=\"";
        echo $this->env->getExtension('routing')->getPath("admin_dashboard");
        echo "\">Kokpit</a></li>
                    <li";
        // line 57
        if (((isset($context["currPage"]) ? $context["currPage"] : null) == "posts")) {
            echo " class=\"active\"";
        }
        echo "><a href=\"";
        echo $this->env->getExtension('routing')->getPath("admin_postsList");
        echo "\">Posty</a></li>
                    
                    ";
        // line 59
        if ($this->env->getExtension('security')->isGranted("ROLE_EDITOR")) {
            // line 60
            echo "                    <li class=\"dropdown";
            if (((isset($context["currPage"]) ? $context["currPage"] : null) == "taxonomies")) {
                echo " active";
            }
            echo "\">
                        <a href=\"#\" class=\"dropdown-toggle\" data-toggle=\"dropdown\">Taksonomie <b class=\"caret\"></b></a>
                        <ul class=\"dropdown-menu\">
                            ";
            // line 63
            if ($this->env->getExtension('security')->isGranted("ROLE_ADMIN")) {
                // line 64
                echo "                            <li><a href=\"";
                echo $this->env->getExtension('routing')->getPath("admin_categoriesList");
                echo "\">Kategorie</a></li>
                            ";
            }
            // line 66
            echo "                            <li><a href=\"";
            echo $this->env->getExtension('routing')->getPath("admin_tagsList");
            echo "\">Tagi</a></li>
                        </ul>
                    </li>
                    ";
        }
        // line 70
        echo "                    
                    ";
        // line 71
        if ($this->env->getExtension('security')->isGranted("ROLE_ADMIN")) {
            // line 72
            echo "                    <li";
            if (((isset($context["currPage"]) ? $context["currPage"] : null) == "users")) {
                echo " class=\"active\"";
            }
            echo "><a href=\"";
            echo $this->env->getExtension('routing')->getPath("user_adminUsersList");
            echo "\">Użytkownicy</a></li>
                    ";
        }
        // line 74
        echo "                </ul>
                
                <ul class=\"nav navbar-nav navbar-right\">
                    ";
        // line 81
        echo "                    <li><a href=\"";
        echo $this->env->getExtension('routing')->getPath("user_accountSettings");
        echo "\">";
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "username", array()), "html", null, true);
        echo "</a></li>
                    <li><a href=\"";
        // line 82
        echo $this->env->getExtension('routing')->getPath("_logout");
        echo "\"><span class=\"glyphicon glyphicon-log-out\"></span></a></li>
                </ul>
            </div>
        </nav>
        
        <!-- alerts container -->
        ";
        // line 88
        if (($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "session", array()), "flashBag", array()), "has", array(0 => "success"), "method") || $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "session", array()), "flashBag", array()), "has", array(0 => "error"), "method"))) {
            // line 89
            echo "        <div class=\"container\">
            <div class=\"row\">
                <div class=\"col-md-12\">
                    
                    ";
            // line 108
            echo "                        
                    ";
            // line 109
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "session", array()), "flashBag", array()), "get", array(0 => "success"), "method"));
            foreach ($context['_seq'] as $context["_key"] => $context["success"]) {
                // line 110
                echo "                    <div class=\"alert alert-success alert-dismissable\">
                        <button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">&times;</button>
                        <strong>Sukces!</strong> ";
                // line 112
                echo twig_escape_filter($this->env, $context["success"], "html", null, true);
                echo "
                    </div>
                    ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['success'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 115
            echo "                    
                    ";
            // line 116
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "session", array()), "flashBag", array()), "get", array(0 => "error"), "method"));
            foreach ($context['_seq'] as $context["_key"] => $context["error"]) {
                // line 117
                echo "                    <div class=\"alert alert-danger alert-dismissable\">
                        <button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">&times;</button>
                        <strong>Błąd!</strong> ";
                // line 119
                echo twig_escape_filter($this->env, $context["error"], "html", null, true);
                echo "
                    </div>
                    ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['error'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 122
            echo "                </div>
            </div>
        </div>
        ";
        }
        // line 126
        echo "        <!-- /alerts container -->

        ";
        // line 128
        if (array_key_exists("form", $context)) {
            // line 129
            echo "            ";
            $this->env->getExtension('form')->renderer->setTheme((isset($context["form"]) ? $context["form"] : null), array(0 => "AirAdminBundle:Form:form_template.html.twig"));
            // line 130
            echo "        ";
        }
        // line 131
        echo "        
        ";
        // line 132
        $this->displayBlock('content', $context, $blocks);
        // line 134
        echo "
    </body>
</html>
";
    }

    // line 9
    public function block_pageTitle($context, array $blocks = array())
    {
        echo "AirBlog Admin Panel";
    }

    // line 13
    public function block_stylesheets($context, array $blocks = array())
    {
        // line 14
        echo "            <link href=\"http://netdna.bootstrapcdn.com/font-awesome/3.2.1/css/font-awesome.min.css\" rel=\"stylesheet\">
            ";
        // line 15
        if (isset($context['assetic']['debug']) && $context['assetic']['debug']) {
            // asset "cd0f765_0"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_cd0f765_0") : $this->env->getExtension('assets')->getAssetUrl("css/cd0f765_part_1_bootstrap-theme_1.css");
            // line 21
            echo "                <link rel=\"stylesheet\" type=\"text/css\" href=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\">
            ";
            // asset "cd0f765_1"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_cd0f765_1") : $this->env->getExtension('assets')->getAssetUrl("css/cd0f765_part_1_bootstrap_2.css");
            echo "                <link rel=\"stylesheet\" type=\"text/css\" href=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\">
            ";
            // asset "cd0f765_2"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_cd0f765_2") : $this->env->getExtension('assets')->getAssetUrl("css/cd0f765_part_2_select2_1.css");
            echo "                <link rel=\"stylesheet\" type=\"text/css\" href=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\">
            ";
            // asset "cd0f765_3"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_cd0f765_3") : $this->env->getExtension('assets')->getAssetUrl("css/cd0f765_blog-admin_3.css");
            echo "                <link rel=\"stylesheet\" type=\"text/css\" href=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\">
            ";
        } else {
            // asset "cd0f765"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_cd0f765") : $this->env->getExtension('assets')->getAssetUrl("css/cd0f765.css");
            echo "                <link rel=\"stylesheet\" type=\"text/css\" href=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\">
            ";
        }
        unset($context["asset_url"]);
        // line 23
        echo "        ";
    }

    // line 25
    public function block_javascripts($context, array $blocks = array())
    {
        // line 26
        echo "            ";
        if (isset($context['assetic']['debug']) && $context['assetic']['debug']) {
            // asset "9c6e0f5_0"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_9c6e0f5_0") : $this->env->getExtension('assets')->getAssetUrl("js/9c6e0f5_jquery-1.10.2.min_1.js");
            // line 32
            echo "                <script src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\"></script>
            ";
            // asset "9c6e0f5_1"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_9c6e0f5_1") : $this->env->getExtension('assets')->getAssetUrl("js/9c6e0f5_part_2_bootstrap_1.js");
            echo "                <script src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\"></script>
            ";
            // asset "9c6e0f5_2"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_9c6e0f5_2") : $this->env->getExtension('assets')->getAssetUrl("js/9c6e0f5_part_2_select2.min_3.js");
            echo "                <script src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\"></script>
            ";
            // asset "9c6e0f5_3"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_9c6e0f5_3") : $this->env->getExtension('assets')->getAssetUrl("js/9c6e0f5_scripts_3.js");
            echo "                <script src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\"></script>
            ";
        } else {
            // asset "9c6e0f5"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_9c6e0f5") : $this->env->getExtension('assets')->getAssetUrl("js/9c6e0f5.js");
            echo "                <script src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\"></script>
            ";
        }
        unset($context["asset_url"]);
        // line 34
        echo "        ";
    }

    // line 132
    public function block_content($context, array $blocks = array())
    {
        // line 133
        echo "        ";
    }

    public function getTemplateName()
    {
        return "AirAdminBundle::base.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  340 => 133,  337 => 132,  333 => 34,  301 => 32,  296 => 26,  293 => 25,  289 => 23,  257 => 21,  253 => 15,  250 => 14,  247 => 13,  241 => 9,  234 => 134,  232 => 132,  229 => 131,  226 => 130,  223 => 129,  221 => 128,  217 => 126,  211 => 122,  202 => 119,  198 => 117,  194 => 116,  191 => 115,  182 => 112,  178 => 110,  174 => 109,  171 => 108,  165 => 89,  163 => 88,  154 => 82,  147 => 81,  142 => 74,  132 => 72,  130 => 71,  127 => 70,  119 => 66,  113 => 64,  111 => 63,  102 => 60,  100 => 59,  91 => 57,  83 => 56,  80 => 55,  76 => 54,  67 => 48,  52 => 35,  50 => 25,  47 => 24,  45 => 13,  40 => 11,  33 => 9,  23 => 1,);
    }
}
