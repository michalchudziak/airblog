<?php

/* AirAdminBundle:Posts:form.html.twig */
class __TwigTemplate_c631a0a4e072dd8d4ebeb5a0c8ae797eebacd096b6634e55a4870281301ce285 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        try {
            $this->parent = $this->env->loadTemplate("AirAdminBundle::base.html.twig");
        } catch (Twig_Error_Loader $e) {
            $e->setTemplateFile($this->getTemplateName());
            $e->setTemplateLine(1);

            throw $e;
        }

        $this->blocks = array(
            'pageTitle' => array($this, 'block_pageTitle'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "AirAdminBundle::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 3
        $context["pageTitle"] = (((null === $this->getAttribute((isset($context["post"]) ? $context["post"] : null), "id", array()))) ? ("Dodawanie nowego wpisu") : (sprintf("Edycja wpisu \"%s\"", $this->getAttribute((isset($context["post"]) ? $context["post"] : null), "title", array()))));
        // line 1
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 5
    public function block_pageTitle($context, array $blocks = array())
    {
        // line 6
        echo "    ";
        echo twig_escape_filter($this->env, (isset($context["pageTitle"]) ? $context["pageTitle"] : null), "html", null, true);
        echo "
";
    }

    // line 9
    public function block_content($context, array $blocks = array())
    {
        // line 10
        echo "    
    <div class=\"container\">

        ";
        // line 13
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["form"]) ? $context["form"] : null), 'form_start');
        echo "
            <div class=\"row\">

                <div class=\"col-md-8\">
                    <div class=\"panel panel-default\">
                        <div class=\"panel-heading\">
                            ";
        // line 19
        echo twig_escape_filter($this->env, (isset($context["pageTitle"]) ? $context["pageTitle"] : null), "html", null, true);
        echo "
                        </div>

                        <div class=\"panel-body\">
                            ";
        // line 23
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "title", array()), 'row');
        echo "
                            ";
        // line 24
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "slug", array()), 'row');
        echo "
                            ";
        // line 25
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "content", array()), 'row');
        echo "
                        </div>
                    </div>
                </div>

                <div class=\"col-md-4\">
                    <div class=\"row\">

                        <div class=\"col-md-12\">
                            <div class=\"panel panel-default\">
                                <div class=\"panel-heading\">
                                    Akcje
                                </div>
                                <div class=\"panel-body\">
                                    ";
        // line 39
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "save", array()), 'row');
        echo "
                                </div>
                            </div>
                        </div> <!-- .col-md-12 -->

                        <div class=\"col-md-12\">
                            <div class=\"panel panel-default\">
                                <div class=\"panel-heading\">
                                    Miniaturka
                                </div>
                                <div class=\"panel-body\">
                                    ";
        // line 50
        if ((null === $this->getAttribute((isset($context["post"]) ? $context["post"] : null), "thumbnail", array()))) {
            // line 51
            echo "                                        <p>Brak miniaturki</p>
                                    ";
        } else {
            // line 53
            echo "                                        <p><img src=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl($this->getAttribute((isset($context["post"]) ? $context["post"] : null), "thumbnail", array())), "html", null, true);
            echo "\" alt=\"\" class=\"img-thumbnail\"></p>
                                    ";
        }
        // line 55
        echo "
                                    ";
        // line 56
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "thumbnailFile", array()), 'widget');
        echo "
                                    ";
        // line 57
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "thumbnailFile", array()), 'errors');
        echo "
                                </div>
                            </div>
                        </div> <!-- .col-md-12 -->

                        <div class=\"col-md-12\">
                            <div class=\"panel panel-default\">
                                <div class=\"panel-heading\">
                                    Właściwości
                                </div>
                                <div class=\"panel-body\">
                                    ";
        // line 68
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "publishedDate", array()), 'row', array("label_length" => 5, "wrapper_length" => 7));
        echo "
                                </div>
                            </div>
                        </div> <!-- .col-md-12 -->

                        <div class=\"col-md-12\">
                            <div class=\"panel panel-default\">
                                <div class=\"panel-heading\">
                                    Taksonomie
                                </div>
                                <div class=\"panel-body\">

                                    ";
        // line 80
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "category", array()), 'row', array("label_length" => 3, "wrapper_length" => 9, "attr" => array("class" => "select-block")));
        echo "

                                    ";
        // line 82
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "tags", array()), 'row', array("label_length" => 3, "wrapper_length" => 9, "attr" => array("class" => "select-block")));
        echo "
                                </div>
                            </div>
                        </div> <!-- .col-md-12 -->

                    </div>
                </div>

            </div>
        ";
        // line 91
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["form"]) ? $context["form"] : null), 'form_end');
        echo "

    </div> <!-- .container -->
";
    }

    public function getTemplateName()
    {
        return "AirAdminBundle:Posts:form.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  178 => 91,  166 => 82,  161 => 80,  146 => 68,  132 => 57,  128 => 56,  125 => 55,  119 => 53,  115 => 51,  113 => 50,  99 => 39,  82 => 25,  78 => 24,  74 => 23,  67 => 19,  58 => 13,  53 => 10,  50 => 9,  43 => 6,  40 => 5,  36 => 1,  34 => 3,  11 => 1,);
    }
}
