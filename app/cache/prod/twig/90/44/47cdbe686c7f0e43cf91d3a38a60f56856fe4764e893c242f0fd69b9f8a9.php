<?php

/* AirBlogBundle:Template:recentCommented.html.twig */
class __TwigTemplate_904447cdbe686c7f0e43cf91d3a38a60f56856fe4764e893c242f0fd69b9f8a9 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div class=\"list-widget\">
    <h3>Najczęściej komentowane</h3>
    <ul>
        ";
        // line 4
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["postsList"]) ? $context["postsList"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["post"]) {
            // line 5
            echo "            <li>
                <a href=\"";
            // line 6
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("blog_post", array("slug" => $this->getAttribute($context["post"], "slug", array(), "array"))), "html", null, true);
            echo "\">
                    ";
            // line 7
            echo twig_escape_filter($this->env, sprintf("%s (%d)", $this->getAttribute($context["post"], "title", array(), "array"), $this->getAttribute($context["post"], "commentsCount", array(), "array")), "html", null, true);
            echo "
                </a>
            </li>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['post'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 11
        echo "    </ul>
</div>";
    }

    public function getTemplateName()
    {
        return "AirBlogBundle:Template:recentCommented.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  45 => 11,  35 => 7,  31 => 6,  28 => 5,  24 => 4,  19 => 1,);
    }
}
