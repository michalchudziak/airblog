<?php

use Symfony\Component\Routing\Exception\MethodNotAllowedException;
use Symfony\Component\Routing\Exception\ResourceNotFoundException;
use Symfony\Component\Routing\RequestContext;

/**
 * appDevUrlMatcher
 *
 * This class has been auto-generated
 * by the Symfony Routing Component.
 */
class appDevUrlMatcher extends Symfony\Bundle\FrameworkBundle\Routing\RedirectableUrlMatcher
{
    /**
     * Constructor.
     */
    public function __construct(RequestContext $context)
    {
        $this->context = $context;
    }

    public function match($pathinfo)
    {
        $allow = array();
        $pathinfo = rawurldecode($pathinfo);
        $context = $this->context;
        $request = $this->request;

        if (0 === strpos($pathinfo, '/assets')) {
            if (0 === strpos($pathinfo, '/assets/css/compiled')) {
                // _assetic_63fad83
                if ($pathinfo === '/assets/css/compiled.css') {
                    return array (  '_controller' => 'assetic.controller:render',  'name' => '63fad83',  'pos' => NULL,  '_format' => 'css',  '_route' => '_assetic_63fad83',);
                }

                if (0 === strpos($pathinfo, '/assets/css/compiled_')) {
                    // _assetic_63fad83_0
                    if ($pathinfo === '/assets/css/compiled_normalize_1.css') {
                        return array (  '_controller' => 'assetic.controller:render',  'name' => '63fad83',  'pos' => 0,  '_format' => 'css',  '_route' => '_assetic_63fad83_0',);
                    }

                    // _assetic_63fad83_1
                    if ($pathinfo === '/assets/css/compiled_main_2.css') {
                        return array (  '_controller' => 'assetic.controller:render',  'name' => '63fad83',  'pos' => 1,  '_format' => 'css',  '_route' => '_assetic_63fad83_1',);
                    }

                }

            }

            if (0 === strpos($pathinfo, '/assets/js/compiled')) {
                // _assetic_6aec9ce
                if ($pathinfo === '/assets/js/compiled.js') {
                    return array (  '_controller' => 'assetic.controller:render',  'name' => '6aec9ce',  'pos' => NULL,  '_format' => 'js',  '_route' => '_assetic_6aec9ce',);
                }

                if (0 === strpos($pathinfo, '/assets/js/compiled_')) {
                    // _assetic_6aec9ce_0
                    if ($pathinfo === '/assets/js/compiled_jquery-1.10.2.min_1.js') {
                        return array (  '_controller' => 'assetic.controller:render',  'name' => '6aec9ce',  'pos' => 0,  '_format' => 'js',  '_route' => '_assetic_6aec9ce_0',);
                    }

                    if (0 === strpos($pathinfo, '/assets/js/compiled_p')) {
                        if (0 === strpos($pathinfo, '/assets/js/compiled_part_2_')) {
                            // _assetic_6aec9ce_1
                            if ($pathinfo === '/assets/js/compiled_part_2_jquery.responsiSlider_2.js') {
                                return array (  '_controller' => 'assetic.controller:render',  'name' => '6aec9ce',  'pos' => 1,  '_format' => 'js',  '_route' => '_assetic_6aec9ce_1',);
                            }

                            // _assetic_6aec9ce_2
                            if ($pathinfo === '/assets/js/compiled_part_2_modernizr-2.7.0.min_3.js') {
                                return array (  '_controller' => 'assetic.controller:render',  'name' => '6aec9ce',  'pos' => 2,  '_format' => 'js',  '_route' => '_assetic_6aec9ce_2',);
                            }

                        }

                        // _assetic_6aec9ce_3
                        if ($pathinfo === '/assets/js/compiled_plugins_3.js') {
                            return array (  '_controller' => 'assetic.controller:render',  'name' => '6aec9ce',  'pos' => 3,  '_format' => 'js',  '_route' => '_assetic_6aec9ce_3',);
                        }

                    }

                    // _assetic_6aec9ce_4
                    if ($pathinfo === '/assets/js/compiled_main_4.js') {
                        return array (  '_controller' => 'assetic.controller:render',  'name' => '6aec9ce',  'pos' => 4,  '_format' => 'js',  '_route' => '_assetic_6aec9ce_4',);
                    }

                }

            }

        }

        if (0 === strpos($pathinfo, '/css/cd0f765')) {
            // _assetic_cd0f765
            if ($pathinfo === '/css/cd0f765.css') {
                return array (  '_controller' => 'assetic.controller:render',  'name' => 'cd0f765',  'pos' => NULL,  '_format' => 'css',  '_route' => '_assetic_cd0f765',);
            }

            if (0 === strpos($pathinfo, '/css/cd0f765_')) {
                if (0 === strpos($pathinfo, '/css/cd0f765_part_')) {
                    if (0 === strpos($pathinfo, '/css/cd0f765_part_1_bootstrap')) {
                        // _assetic_cd0f765_0
                        if ($pathinfo === '/css/cd0f765_part_1_bootstrap-theme_1.css') {
                            return array (  '_controller' => 'assetic.controller:render',  'name' => 'cd0f765',  'pos' => 0,  '_format' => 'css',  '_route' => '_assetic_cd0f765_0',);
                        }

                        // _assetic_cd0f765_1
                        if ($pathinfo === '/css/cd0f765_part_1_bootstrap_2.css') {
                            return array (  '_controller' => 'assetic.controller:render',  'name' => 'cd0f765',  'pos' => 1,  '_format' => 'css',  '_route' => '_assetic_cd0f765_1',);
                        }

                    }

                    // _assetic_cd0f765_2
                    if ($pathinfo === '/css/cd0f765_part_2_select2_1.css') {
                        return array (  '_controller' => 'assetic.controller:render',  'name' => 'cd0f765',  'pos' => 2,  '_format' => 'css',  '_route' => '_assetic_cd0f765_2',);
                    }

                }

                // _assetic_cd0f765_3
                if ($pathinfo === '/css/cd0f765_blog-admin_3.css') {
                    return array (  '_controller' => 'assetic.controller:render',  'name' => 'cd0f765',  'pos' => 3,  '_format' => 'css',  '_route' => '_assetic_cd0f765_3',);
                }

            }

        }

        if (0 === strpos($pathinfo, '/js/9c6e0f5')) {
            // _assetic_9c6e0f5
            if ($pathinfo === '/js/9c6e0f5.js') {
                return array (  '_controller' => 'assetic.controller:render',  'name' => '9c6e0f5',  'pos' => NULL,  '_format' => 'js',  '_route' => '_assetic_9c6e0f5',);
            }

            if (0 === strpos($pathinfo, '/js/9c6e0f5_')) {
                // _assetic_9c6e0f5_0
                if ($pathinfo === '/js/9c6e0f5_jquery-1.10.2.min_1.js') {
                    return array (  '_controller' => 'assetic.controller:render',  'name' => '9c6e0f5',  'pos' => 0,  '_format' => 'js',  '_route' => '_assetic_9c6e0f5_0',);
                }

                if (0 === strpos($pathinfo, '/js/9c6e0f5_part_2_')) {
                    // _assetic_9c6e0f5_1
                    if ($pathinfo === '/js/9c6e0f5_part_2_bootstrap_1.js') {
                        return array (  '_controller' => 'assetic.controller:render',  'name' => '9c6e0f5',  'pos' => 1,  '_format' => 'js',  '_route' => '_assetic_9c6e0f5_1',);
                    }

                    // _assetic_9c6e0f5_2
                    if ($pathinfo === '/js/9c6e0f5_part_2_select2.min_3.js') {
                        return array (  '_controller' => 'assetic.controller:render',  'name' => '9c6e0f5',  'pos' => 2,  '_format' => 'js',  '_route' => '_assetic_9c6e0f5_2',);
                    }

                }

                // _assetic_9c6e0f5_3
                if ($pathinfo === '/js/9c6e0f5_scripts_3.js') {
                    return array (  '_controller' => 'assetic.controller:render',  'name' => '9c6e0f5',  'pos' => 3,  '_format' => 'js',  '_route' => '_assetic_9c6e0f5_3',);
                }

            }

        }

        if (0 === strpos($pathinfo, '/_')) {
            // _wdt
            if (0 === strpos($pathinfo, '/_wdt') && preg_match('#^/_wdt/(?P<token>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => '_wdt')), array (  '_controller' => 'web_profiler.controller.profiler:toolbarAction',));
            }

            if (0 === strpos($pathinfo, '/_profiler')) {
                // _profiler_home
                if (rtrim($pathinfo, '/') === '/_profiler') {
                    if (substr($pathinfo, -1) !== '/') {
                        return $this->redirect($pathinfo.'/', '_profiler_home');
                    }

                    return array (  '_controller' => 'web_profiler.controller.profiler:homeAction',  '_route' => '_profiler_home',);
                }

                if (0 === strpos($pathinfo, '/_profiler/search')) {
                    // _profiler_search
                    if ($pathinfo === '/_profiler/search') {
                        return array (  '_controller' => 'web_profiler.controller.profiler:searchAction',  '_route' => '_profiler_search',);
                    }

                    // _profiler_search_bar
                    if ($pathinfo === '/_profiler/search_bar') {
                        return array (  '_controller' => 'web_profiler.controller.profiler:searchBarAction',  '_route' => '_profiler_search_bar',);
                    }

                }

                // _profiler_purge
                if ($pathinfo === '/_profiler/purge') {
                    return array (  '_controller' => 'web_profiler.controller.profiler:purgeAction',  '_route' => '_profiler_purge',);
                }

                // _profiler_info
                if (0 === strpos($pathinfo, '/_profiler/info') && preg_match('#^/_profiler/info/(?P<about>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler_info')), array (  '_controller' => 'web_profiler.controller.profiler:infoAction',));
                }

                // _profiler_phpinfo
                if ($pathinfo === '/_profiler/phpinfo') {
                    return array (  '_controller' => 'web_profiler.controller.profiler:phpinfoAction',  '_route' => '_profiler_phpinfo',);
                }

                // _profiler_search_results
                if (preg_match('#^/_profiler/(?P<token>[^/]++)/search/results$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler_search_results')), array (  '_controller' => 'web_profiler.controller.profiler:searchResultsAction',));
                }

                // _profiler
                if (preg_match('#^/_profiler/(?P<token>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler')), array (  '_controller' => 'web_profiler.controller.profiler:panelAction',));
                }

                // _profiler_router
                if (preg_match('#^/_profiler/(?P<token>[^/]++)/router$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler_router')), array (  '_controller' => 'web_profiler.controller.router:panelAction',));
                }

                // _profiler_exception
                if (preg_match('#^/_profiler/(?P<token>[^/]++)/exception$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler_exception')), array (  '_controller' => 'web_profiler.controller.exception:showAction',));
                }

                // _profiler_exception_css
                if (preg_match('#^/_profiler/(?P<token>[^/]++)/exception\\.css$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler_exception_css')), array (  '_controller' => 'web_profiler.controller.exception:cssAction',));
                }

            }

            if (0 === strpos($pathinfo, '/_configurator')) {
                // _configurator_home
                if (rtrim($pathinfo, '/') === '/_configurator') {
                    if (substr($pathinfo, -1) !== '/') {
                        return $this->redirect($pathinfo.'/', '_configurator_home');
                    }

                    return array (  '_controller' => 'Sensio\\Bundle\\DistributionBundle\\Controller\\ConfiguratorController::checkAction',  '_route' => '_configurator_home',);
                }

                // _configurator_step
                if (0 === strpos($pathinfo, '/_configurator/step') && preg_match('#^/_configurator/step/(?P<index>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_configurator_step')), array (  '_controller' => 'Sensio\\Bundle\\DistributionBundle\\Controller\\ConfiguratorController::stepAction',));
                }

                // _configurator_final
                if ($pathinfo === '/_configurator/final') {
                    return array (  '_controller' => 'Sensio\\Bundle\\DistributionBundle\\Controller\\ConfiguratorController::finalAction',  '_route' => '_configurator_final',);
                }

            }

        }

        // blog_login
        if ($pathinfo === '/login') {
            return array (  '_controller' => 'Common\\UserBundle\\Controller\\LoginController::loginAction',  '_route' => 'blog_login',);
        }

        // user_resetPassword
        if (0 === strpos($pathinfo, '/reset-password') && preg_match('#^/reset\\-password/(?P<actionToken>[^/]++)$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'user_resetPassword')), array (  '_controller' => 'Common\\UserBundle\\Controller\\LoginController::resetPasswordAction',));
        }

        if (0 === strpos($pathinfo, '/account-')) {
            // user_activateAccount
            if (0 === strpos($pathinfo, '/account-activation') && preg_match('#^/account\\-activation/(?P<actionToken>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'user_activateAccount')), array (  '_controller' => 'Common\\UserBundle\\Controller\\LoginController::activateAccountAction',));
            }

            // user_accountSettings
            if ($pathinfo === '/account-settings') {
                return array (  '_controller' => 'Common\\UserBundle\\Controller\\UserController::accountSettingsAction',  '_route' => 'user_accountSettings',);
            }

        }

        if (0 === strpos($pathinfo, '/log')) {
            // _check_path
            if ($pathinfo === '/login-check') {
                return array('_route' => '_check_path');
            }

            // _logout
            if ($pathinfo === '/logout') {
                return array('_route' => '_logout');
            }

        }

        if (0 === strpos($pathinfo, '/a')) {
            if (0 === strpos($pathinfo, '/admin-panel/users')) {
                // user_adminUsersList
                if (0 === strpos($pathinfo, '/admin-panel/users/list') && preg_match('#^/admin\\-panel/users/list(?:/(?P<page>\\d+))?$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'user_adminUsersList')), array (  'page' => 1,  '_controller' => 'Common\\UserBundle\\Controller\\AdminController::indexAction',));
                }

                // user_adminUserForm
                if (0 === strpos($pathinfo, '/admin-panel/users/form') && preg_match('#^/admin\\-panel/users/form/(?P<id>\\d+)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'user_adminUserForm')), array (  '_controller' => 'Common\\UserBundle\\Controller\\AdminController::formAction',));
                }

            }

            // blog_about
            if ($pathinfo === '/about') {
                return array (  '_controller' => 'Symfony\\Bundle\\FrameworkBundle\\Controller\\TemplateController::templateAction',  'template' => 'AirBlogBundle:Pages:about.html.twig',  '_route' => 'blog_about',);
            }

        }

        // blog_contact
        if ($pathinfo === '/contact') {
            return array (  '_controller' => 'Air\\BlogBundle\\Controller\\PagesController::contactAction',  '_route' => 'blog_contact',);
        }

        // blog_index
        if (preg_match('#^/(?P<page>\\d+)?$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'blog_index')), array (  'page' => 1,  '_controller' => 'Air\\BlogBundle\\Controller\\PostsController::indexAction',));
        }

        // blog_search
        if (0 === strpos($pathinfo, '/search') && preg_match('#^/search(?:/(?P<page>\\d+))?$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'blog_search')), array (  'page' => 1,  '_controller' => 'Air\\BlogBundle\\Controller\\PostsController::searchAction',));
        }

        // blog_post
        if (preg_match('#^/(?P<slug>[^/]++)$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'blog_post')), array (  '_controller' => 'Air\\BlogBundle\\Controller\\PostsController::postAction',));
        }

        // blog_deleteComment
        if (0 === strpos($pathinfo, '/post/comment/delete') && preg_match('#^/post/comment/delete/(?P<commentId>[^/]++)/(?P<token>[^/]++)$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'blog_deleteComment')), array (  '_controller' => 'Air\\BlogBundle\\Controller\\PostsController::deleteCommentAction',));
        }

        // blog_category
        if (0 === strpos($pathinfo, '/category') && preg_match('#^/category/(?P<slug>[^/]++)(?:/(?P<page>\\d+))?$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'blog_category')), array (  'page' => 1,  '_controller' => 'Air\\BlogBundle\\Controller\\PostsController::categoryAction',));
        }

        // blog_tag
        if (0 === strpos($pathinfo, '/tag') && preg_match('#^/tag/(?P<slug>[^/]++)(?:/(?P<page>\\d+))?$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'blog_tag')), array (  'page' => 1,  '_controller' => 'Air\\BlogBundle\\Controller\\PostsController::tagAction',));
        }

        if (0 === strpos($pathinfo, '/admin-panel')) {
            // admin_dashboard
            if (rtrim($pathinfo, '/') === '/admin-panel/dashboard') {
                if (substr($pathinfo, -1) !== '/') {
                    return $this->redirect($pathinfo.'/', 'admin_dashboard');
                }

                return array (  '_controller' => 'Air\\AdminBundle\\Controller\\DashboardController::indexAction',  '_route' => 'admin_dashboard',);
            }

            if (0 === strpos($pathinfo, '/admin-panel/categories')) {
                // admin_categoriesList
                if (0 === strpos($pathinfo, '/admin-panel/categories/list') && preg_match('#^/admin\\-panel/categories/list(?:/(?P<page>[^/]++))?$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_categoriesList')), array (  'page' => 1,  '_controller' => 'Air\\AdminBundle\\Controller\\CategoriesController::indexAction',));
                }

                // admin_categoryForm
                if (0 === strpos($pathinfo, '/admin-panel/categories/form') && preg_match('#^/admin\\-panel/categories/form(?:/(?P<id>\\d+))?$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_categoryForm')), array (  'id' => NULL,  'Category' => NULL,  '_controller' => 'Air\\AdminBundle\\Controller\\CategoriesController::formAction',));
                }

                // admin_categoryDelete
                if (0 === strpos($pathinfo, '/admin-panel/categories/delete') && preg_match('#^/admin\\-panel/categories/delete/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_categoryDelete')), array (  '_controller' => 'Air\\AdminBundle\\Controller\\CategoriesController::deleteAction',));
                }

            }

            if (0 === strpos($pathinfo, '/admin-panel/posts')) {
                // admin_postsList
                if (0 === strpos($pathinfo, '/admin-panel/posts/list') && preg_match('#^/admin\\-panel/posts/list(?:/(?P<status>[^/]++)(?:/(?P<page>\\d+))?)?$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_postsList')), array (  'status' => 'all',  'page' => 1,  '_controller' => 'Air\\AdminBundle\\Controller\\PostsController::indexAction',));
                }

                // admin_postForm
                if (0 === strpos($pathinfo, '/admin-panel/posts/form') && preg_match('#^/admin\\-panel/posts/form(?:/(?P<id>\\d+))?$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_postForm')), array (  'id' => NULL,  '_controller' => 'Air\\AdminBundle\\Controller\\PostsController::formAction',));
                }

                // admin_postDelete
                if (0 === strpos($pathinfo, '/admin-panel/posts/delete') && preg_match('#^/admin\\-panel/posts/delete/(?P<id>\\d+)/(?P<token>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_postDelete')), array (  '_controller' => 'Air\\AdminBundle\\Controller\\PostsController::deleteAction',));
                }

            }

            if (0 === strpos($pathinfo, '/admin-panel/tags')) {
                // admin_tagsList
                if (0 === strpos($pathinfo, '/admin-panel/tags/list') && preg_match('#^/admin\\-panel/tags/list(?:/(?P<page>\\d+))?$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_tagsList')), array (  'page' => 1,  '_controller' => 'Air\\AdminBundle\\Controller\\TagsController::indexAction',));
                }

                // admin_tagForm
                if (0 === strpos($pathinfo, '/admin-panel/tags/form') && preg_match('#^/admin\\-panel/tags/form(?:/(?P<id>\\d+))?$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_tagForm')), array (  'id' => NULL,  'Tag' => NULL,  '_controller' => 'Air\\AdminBundle\\Controller\\TagsController::formAction',));
                }

                // admin_tagDelete
                if (0 === strpos($pathinfo, '/admin-panel/tags/delete') && preg_match('#^/admin\\-panel/tags/delete/(?P<id>\\d+)/(?P<token>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_tagDelete')), array (  '_controller' => 'Air\\AdminBundle\\Controller\\TagsController::deleteAction',));
                }

            }

        }

        throw 0 < count($allow) ? new MethodNotAllowedException(array_unique($allow)) : new ResourceNotFoundException();
    }
}
