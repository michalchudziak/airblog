<?php

/* AirBlogBundle:Email:contact.html.twig */
class __TwigTemplate_f36c941c156261d34dbc5172cb6a4a887dbb6a5fb4c390d276f519ce5b628a63 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        try {
            $this->parent = $this->env->loadTemplate("CommonUserBundle:Email:base.html.twig");
        } catch (Twig_Error_Loader $e) {
            $e->setTemplateFile($this->getTemplateName());
            $e->setTemplateLine(1);

            throw $e;
        }

        $this->blocks = array(
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "CommonUserBundle:Email:base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_content($context, array $blocks = array())
    {
        // line 4
        echo "<tr>
    <td style=\"width: 25px;\"></td>
    <td style=\"font-family: 'Trebuchet MS', Helvetica, sans-serif; font-size: 21px; color: #484848;\">
        Wiadomość od: ";
        // line 7
        echo twig_escape_filter($this->env, (isset($context["name"]) ? $context["name"] : $this->getContext($context, "name")), "html", null, true);
        echo " (";
        echo twig_escape_filter($this->env, (isset($context["email"]) ? $context["email"] : $this->getContext($context, "email")), "html", null, true);
        echo ")
    </td>
    <td style=\"width: 25px;\"></td>
</tr>
<tr>
    <td style=\"width: 25px;\"></td>
    <td style=\"font-family: 'Trebuchet MS', Helvetica, sans-serif; font-size: 21px; color: #484848;\">
        ";
        // line 14
        echo twig_escape_filter($this->env, (isset($context["message"]) ? $context["message"] : $this->getContext($context, "message")), "html", null, true);
        echo "
    </td>
    <td style=\"width: 25px;\"></td>
</tr>
";
    }

    public function getTemplateName()
    {
        return "AirBlogBundle:Email:contact.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  56 => 14,  44 => 7,  39 => 4,  36 => 3,  11 => 1,);
    }
}
