<?php

/* CommonUserBundle:User:accountSettings.html.twig */
class __TwigTemplate_b63696f2e4f0cdd91de1b30a62d343c83dd9c3e9f01f3d740b10fd66be34e5be extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        try {
            $this->parent = $this->env->loadTemplate("AirBlogBundle::base.html.twig");
        } catch (Twig_Error_Loader $e) {
            $e->setTemplateFile($this->getTemplateName());
            $e->setTemplateLine(1);

            throw $e;
        }

        $this->blocks = array(
            'pageTitle' => array($this, 'block_pageTitle'),
            'contentTitle' => array($this, 'block_contentTitle'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "AirBlogBundle::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 3
        $context["hideSidebar"] = true;
        // line 4
        $context["hideSearch"] = true;
        // line 1
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 6
    public function block_pageTitle($context, array $blocks = array())
    {
        echo "Ustawienia konta";
    }

    // line 8
    public function block_contentTitle($context, array $blocks = array())
    {
        echo "Ustawienia konta";
    }

    // line 10
    public function block_content($context, array $blocks = array())
    {
        // line 11
        echo "
    ";
        // line 12
        $this->env->loadTemplate("AirBlogBundle:Template:flashMsg.html.twig")->display($context);
        // line 13
        echo "    
<div class=\"two-cols account-settings\">
    <div class=\"col\">
        <h2 class=\"strong\">Zmień dane konta</h2>

        ";
        // line 34
        echo "        
        ";
        // line 35
        $this->env->getExtension('form')->renderer->setTheme((isset($context["accountSettingsForm"]) ? $context["accountSettingsForm"] : $this->getContext($context, "accountSettingsForm")), array(0 => "AirBlogBundle:Form:form_template.html.twig"));
        // line 36
        echo "        ";
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["accountSettingsForm"]) ? $context["accountSettingsForm"] : $this->getContext($context, "accountSettingsForm")), 'form_start');
        echo "
        
            <div class=\"avatar-row\">
                <img src=\"";
        // line 39
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl($this->getAttribute((isset($context["user"]) ? $context["user"] : $this->getContext($context, "user")), "avatar", array())), "html", null, true);
        echo "\" alt=\"\">
                ";
        // line 40
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["accountSettingsForm"]) ? $context["accountSettingsForm"] : $this->getContext($context, "accountSettingsForm")), "avatarFile", array()), 'row');
        echo "
            </div>
            
            ";
        // line 43
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["accountSettingsForm"]) ? $context["accountSettingsForm"] : $this->getContext($context, "accountSettingsForm")), 'rest');
        echo "
        
        ";
        // line 45
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["accountSettingsForm"]) ? $context["accountSettingsForm"] : $this->getContext($context, "accountSettingsForm")), 'form_end');
        echo "
        
    </div>

    <div class=\"col col-bdl\">
        <h2 class=\"strong\">Zmiana hasła</h2>

        ";
        // line 72
        echo "        
        ";
        // line 73
        $this->env->getExtension('form')->renderer->setTheme((isset($context["changePasswdForm"]) ? $context["changePasswdForm"] : $this->getContext($context, "changePasswdForm")), array(0 => "AirBlogBundle:Form:form_template.html.twig"));
        // line 74
        echo "        ";
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["changePasswdForm"]) ? $context["changePasswdForm"] : $this->getContext($context, "changePasswdForm")), 'form');
        echo "
        
    </div>
</div>
";
    }

    public function getTemplateName()
    {
        return "CommonUserBundle:User:accountSettings.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  112 => 74,  110 => 73,  107 => 72,  97 => 45,  92 => 43,  86 => 40,  82 => 39,  75 => 36,  73 => 35,  70 => 34,  63 => 13,  61 => 12,  58 => 11,  55 => 10,  49 => 8,  43 => 6,  39 => 1,  37 => 4,  35 => 3,  11 => 1,);
    }
}
