<?php

/* AirAdminBundle:Tags:index.html.twig */
class __TwigTemplate_fa0bfced93df27d708077d320bafd1adba246f6657e711c1374eeff8a995cd37 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        try {
            $this->parent = $this->env->loadTemplate("AirAdminBundle::base.html.twig");
        } catch (Twig_Error_Loader $e) {
            $e->setTemplateFile($this->getTemplateName());
            $e->setTemplateLine(1);

            throw $e;
        }

        $this->blocks = array(
            'pageTitle' => array($this, 'block_pageTitle'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "AirAdminBundle::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_pageTitle($context, array $blocks = array())
    {
        // line 4
        echo "\t";
        $this->displayParentBlock("pageTitle", $context, $blocks);
        echo " - Tagi
";
    }

    // line 7
    public function block_content($context, array $blocks = array())
    {
        // line 8
        echo "<div class=\"container\">
    <div class=\"row\">
        <div class=\"col-md-12\">

            <div class=\"panel panel-default\">
                <div class=\"panel-heading\">Tagi</div>
                <div class=\"panel-body\">

                    <ul class=\"nav nav-pills\">
                        <li><a href=\"";
        // line 17
        echo $this->env->getExtension('routing')->getPath("admin_tagForm");
        echo "\">Dodaj nowy</a></li>
                    </ul>

                    <div class=\"clearfix\"></div>

                    <div style=\"margin-top: 20px;\" class=\"table-responsive\">
                        <table class=\"table table-striped table-bordered table-hover\">
                            <thead>
                                <tr>
                                    <th>";
        // line 26
        echo $this->env->getExtension('knp_pagination')->sortable((isset($context["pagination"]) ? $context["pagination"] : $this->getContext($context, "pagination")), "ID", "t.id");
        echo "</th>
                                    <th>";
        // line 27
        echo $this->env->getExtension('knp_pagination')->sortable((isset($context["pagination"]) ? $context["pagination"] : $this->getContext($context, "pagination")), "Nazwa", "p.name");
        echo "</th>
                                    <th>";
        // line 28
        echo $this->env->getExtension('knp_pagination')->sortable((isset($context["pagination"]) ? $context["pagination"] : $this->getContext($context, "pagination")), "Ilość postów przypisanych", "postsCount");
        echo "</th>
                                    <th class=\"actions\">Akcje</th>
                                </tr>
                            </thead>
                            <tbody>
                                ";
        // line 33
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["pagination"]) ? $context["pagination"] : $this->getContext($context, "pagination")));
        foreach ($context['_seq'] as $context["_key"] => $context["row"]) {
            // line 34
            echo "                                    ";
            $context["tag"] = $this->getAttribute($context["row"], 0, array(), "array");
            // line 35
            echo "                                    ";
            $context["editPath"] = $this->env->getExtension('routing')->getPath("admin_tagForm", array("id" => $this->getAttribute((isset($context["tag"]) ? $context["tag"] : $this->getContext($context, "tag")), "id", array())));
            // line 36
            echo "                                    <tr>
                                        <td>";
            // line 37
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["tag"]) ? $context["tag"] : $this->getContext($context, "tag")), "id", array()), "html", null, true);
            echo "</td>
                                        <td><a href=\"";
            // line 38
            echo twig_escape_filter($this->env, (isset($context["editPath"]) ? $context["editPath"] : $this->getContext($context, "editPath")), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["tag"]) ? $context["tag"] : $this->getContext($context, "tag")), "name", array()), "html", null, true);
            echo "</a></td>
                                        <td>";
            // line 39
            echo twig_escape_filter($this->env, $this->getAttribute($context["row"], "postsCount", array(), "array"), "html", null, true);
            echo "</td>
                                        <td>
                                            <div class=\"btn-group\">
                                                <a href=\"";
            // line 42
            echo twig_escape_filter($this->env, (isset($context["editPath"]) ? $context["editPath"] : $this->getContext($context, "editPath")), "html", null, true);
            echo "\" class=\"btn btn-primary\"><span class=\"glyphicon glyphicon-edit\"></span></a>

                                                ";
            // line 44
            if ($this->env->getExtension('security')->isGranted("ROLE_ADMIN")) {
                // line 45
                echo "                                                    ";
                $context["tokenName"] = $this->getAttribute((isset($context["csrfProvider"]) ? $context["csrfProvider"] : $this->getContext($context, "csrfProvider")), "generateCsrfToken", array(0 => sprintf((isset($context["deleteTokenName"]) ? $context["deleteTokenName"] : $this->getContext($context, "deleteTokenName")), $this->getAttribute((isset($context["tag"]) ? $context["tag"] : $this->getContext($context, "tag")), "id", array()))), "method");
                // line 46
                echo "                                                    ";
                $context["deletePath"] = $this->env->getExtension('routing')->getPath("admin_tagDelete", array("id" => $this->getAttribute((isset($context["tag"]) ? $context["tag"] : $this->getContext($context, "tag")), "id", array()), "token" => (isset($context["tokenName"]) ? $context["tokenName"] : $this->getContext($context, "tokenName"))));
                // line 47
                echo "                                                    <a href=\"";
                echo twig_escape_filter($this->env, (isset($context["deletePath"]) ? $context["deletePath"] : $this->getContext($context, "deletePath")), "html", null, true);
                echo "\" class=\"btn btn-danger\" data-confirmAction=\"\">
                                                        <span class=\"glyphicon glyphicon-trash\"></span>
                                                    </a>
                                                ";
            }
            // line 51
            echo "                                            </div>
                                        </td>
                                    </tr>
                                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['row'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 55
        echo "                            </tbody>
                        </table>
                    </div>

                    ";
        // line 59
        echo $this->env->getExtension('knp_pagination')->render((isset($context["pagination"]) ? $context["pagination"] : $this->getContext($context, "pagination")), "AirAdminBundle:Pagination:admin_pagination.html.twig");
        echo "
                </div>  <!-- .panel-body -->
            </div>  <!-- .panel -->
        </div> <!-- .col-md-12 -->
    </div> <!-- .row -->
</div> <!-- .container -->
";
    }

    public function getTemplateName()
    {
        return "AirAdminBundle:Tags:index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  154 => 59,  148 => 55,  139 => 51,  131 => 47,  128 => 46,  125 => 45,  123 => 44,  118 => 42,  112 => 39,  106 => 38,  102 => 37,  99 => 36,  96 => 35,  93 => 34,  89 => 33,  81 => 28,  77 => 27,  73 => 26,  61 => 17,  50 => 8,  47 => 7,  40 => 4,  37 => 3,  11 => 1,);
    }
}
