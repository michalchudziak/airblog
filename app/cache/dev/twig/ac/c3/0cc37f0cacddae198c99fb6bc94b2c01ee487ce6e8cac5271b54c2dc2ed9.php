<?php

/* AirBlogBundle:Template:mainMenu.html.twig */
class __TwigTemplate_acc30cc37f0cacddae198c99fb6bc94b2c01ee487ce6e8cac5271b54c2dc2ed9 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<ul>
    ";
        // line 2
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["mainMenu"]) ? $context["mainMenu"] : $this->getContext($context, "mainMenu")));
        foreach ($context['_seq'] as $context["name"] => $context["route"]) {
            // line 3
            echo "        
    ";
            // line 4
            if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "request", array()), "get", array(0 => "_route"), "method") == $context["route"])) {
                // line 5
                echo "        <li class=\"active\">
    ";
            } else {
                // line 7
                echo "        <li>
    ";
            }
            // line 9
            echo "        <a href=\"";
            echo $this->env->getExtension('routing')->getPath($context["route"]);
            echo "\">
            ";
            // line 10
            echo twig_escape_filter($this->env, $context["name"], "html", null, true);
            echo "
        </a>
    </li>
    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['name'], $context['route'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 14
        echo "</ul>";
    }

    public function getTemplateName()
    {
        return "AirBlogBundle:Template:mainMenu.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  54 => 14,  44 => 10,  39 => 9,  35 => 7,  31 => 5,  29 => 4,  26 => 3,  22 => 2,  19 => 1,);
    }
}
