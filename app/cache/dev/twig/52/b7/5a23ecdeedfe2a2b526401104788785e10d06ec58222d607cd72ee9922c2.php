<?php

/* CommonUserBundle:Login:login.html.twig */
class __TwigTemplate_52b75a23ecdeedfe2a2b526401104788785e10d06ec58222d607cd72ee9922c2 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        try {
            $this->parent = $this->env->loadTemplate("AirBlogBundle::base.html.twig");
        } catch (Twig_Error_Loader $e) {
            $e->setTemplateFile($this->getTemplateName());
            $e->setTemplateLine(1);

            throw $e;
        }

        $this->blocks = array(
            'pageTitle' => array($this, 'block_pageTitle'),
            'contentTitle' => array($this, 'block_contentTitle'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "AirBlogBundle::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 3
        $context["hideSidebar"] = true;
        // line 4
        $context["hideSearch"] = true;
        // line 1
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 6
    public function block_pageTitle($context, array $blocks = array())
    {
        echo "Zaloguj się lub zarejestruj";
    }

    // line 8
    public function block_contentTitle($context, array $blocks = array())
    {
        echo "Logowanie i rejestracja";
    }

    // line 10
    public function block_content($context, array $blocks = array())
    {
        // line 11
        echo "
    ";
        // line 12
        $this->env->loadTemplate("AirBlogBundle:Template:flashMsg.html.twig")->display($context);
        // line 13
        echo "    
<div class=\"login-register two-cols\">
    <div class=\"col col-bdr\">

        <h2 class=\"strong\">Logowanie</h2>

        ";
        // line 37
        echo "        ";
        $this->env->getExtension('form')->renderer->setTheme((isset($context["loginForm"]) ? $context["loginForm"] : $this->getContext($context, "loginForm")), array(0 => "AirBlogBundle:Form:form_template.html.twig"));
        // line 38
        echo "        ";
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["loginForm"]) ? $context["loginForm"] : $this->getContext($context, "loginForm")), 'form', array("action" => $this->env->getExtension('routing')->getPath("_check_path")));
        echo "

        <div class=\"remember-pass-form\">
            <h2 class=\"strong\">Przypomnij hasło</h2>

            ";
        // line 52
        echo "            ";
        $this->env->getExtension('form')->renderer->setTheme((isset($context["rememberPasswdForm"]) ? $context["rememberPasswdForm"] : $this->getContext($context, "rememberPasswdForm")), array(0 => "AirBlogBundle:Form:form_template.html.twig"));
        // line 53
        echo "            ";
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["rememberPasswdForm"]) ? $context["rememberPasswdForm"] : $this->getContext($context, "rememberPasswdForm")), 'form');
        echo "
        </div>

    </div>

    <div class=\"col\">

        <h2 class=\"strong\">Rejestracja</h2>

        ";
        // line 86
        echo "        ";
        $this->env->getExtension('form')->renderer->setTheme((isset($context["registerUserForm"]) ? $context["registerUserForm"] : $this->getContext($context, "registerUserForm")), array(0 => "AirBlogBundle:Form:form_template.html.twig"));
        // line 87
        echo "        ";
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["registerUserForm"]) ? $context["registerUserForm"] : $this->getContext($context, "registerUserForm")), 'form');
        echo "
    </div>
</div>
";
    }

    public function getTemplateName()
    {
        return "CommonUserBundle:Login:login.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  102 => 87,  99 => 86,  86 => 53,  83 => 52,  74 => 38,  71 => 37,  63 => 13,  61 => 12,  58 => 11,  55 => 10,  49 => 8,  43 => 6,  39 => 1,  37 => 4,  35 => 3,  11 => 1,);
    }
}
