<?php

/* CommonUserBundle:Admin:index.html.twig */
class __TwigTemplate_dc057e90ae6fab16dc76ac4c3e1dd5ec362f07498ab3a234bd78842bcd5757b5 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        try {
            $this->parent = $this->env->loadTemplate("AirAdminBundle::base.html.twig");
        } catch (Twig_Error_Loader $e) {
            $e->setTemplateFile($this->getTemplateName());
            $e->setTemplateLine(1);

            throw $e;
        }

        $this->blocks = array(
            'pageTitle' => array($this, 'block_pageTitle'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "AirAdminBundle::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_pageTitle($context, array $blocks = array())
    {
        // line 4
        echo "\t";
        $this->displayParentBlock("pageTitle", $context, $blocks);
        echo " - Posty
";
    }

    // line 7
    public function block_content($context, array $blocks = array())
    {
        // line 8
        echo "<div class=\"container\">
    <div class=\"row\">
        <div class=\"col-md-12\">

            <div class=\"panel panel-default\">
                <div class=\"panel-heading\">
                    Użytkownicy
                </div>
                <div class=\"panel-body\">

                    <div class=\"table-responsive\">
                        <table class=\"table table-striped table-bordered table-hover\">
                            <thead>
                                <tr>
                                    ";
        // line 29
        echo "                                    <th>";
        echo $this->env->getExtension('knp_pagination')->sortable((isset($context["pagination"]) ? $context["pagination"] : $this->getContext($context, "pagination")), "Id", "u.id");
        echo "</th>
                                    <th>";
        // line 30
        echo $this->env->getExtension('knp_pagination')->sortable((isset($context["pagination"]) ? $context["pagination"] : $this->getContext($context, "pagination")), "Nick", "u.username");
        echo "</th>
                                    <th>";
        // line 31
        echo $this->env->getExtension('knp_pagination')->sortable((isset($context["pagination"]) ? $context["pagination"] : $this->getContext($context, "pagination")), "Email", "u.email");
        echo "</th>
                                    <th>";
        // line 32
        echo $this->env->getExtension('knp_pagination')->sortable((isset($context["pagination"]) ? $context["pagination"] : $this->getContext($context, "pagination")), "Data rejestracji", "u.registerDate");
        echo "</th>
                                    <th>Akcje</th>
                                </tr>
                            </thead>
                            <tbody>
                                ";
        // line 37
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["pagination"]) ? $context["pagination"] : $this->getContext($context, "pagination")));
        foreach ($context['_seq'] as $context["_key"] => $context["user"]) {
            // line 38
            echo "                                    ";
            $context["formPath"] = $this->env->getExtension('routing')->getPath("user_adminUserForm", array("id" => $this->getAttribute($context["user"], "id", array())));
            // line 39
            echo "                                <tr>
                                    <td>";
            // line 40
            echo twig_escape_filter($this->env, $this->getAttribute($context["user"], "id", array()), "html", null, true);
            echo "</td>
                                    <td><a href=\"";
            // line 41
            echo twig_escape_filter($this->env, (isset($context["formPath"]) ? $context["formPath"] : $this->getContext($context, "formPath")), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->getAttribute($context["user"], "username", array()), "html", null, true);
            echo "</a></td>
                                    <td>";
            // line 42
            echo twig_escape_filter($this->env, $this->getAttribute($context["user"], "email", array()), "html", null, true);
            echo "</td>
                                    <td>";
            // line 43
            echo twig_escape_filter($this->env, $this->env->getExtension('air_admin_extension')->adminFormatDate($this->getAttribute($context["user"], "registerDate", array())), "html", null, true);
            echo "</td>
                                    <td>
                                        <a href=\"";
            // line 45
            echo twig_escape_filter($this->env, (isset($context["formPath"]) ? $context["formPath"] : $this->getContext($context, "formPath")), "html", null, true);
            echo "\" class=\"btn btn-primary\"><span class=\"glyphicon glyphicon-edit\"></span></a>
                                    </td>
                                </tr>
                                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['user'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 49
        echo "                            </tbody>
                        </table>
                    </div>
                    
                    ";
        // line 53
        echo $this->env->getExtension('knp_pagination')->render((isset($context["pagination"]) ? $context["pagination"] : $this->getContext($context, "pagination")), "AirAdminBundle:Pagination:admin_pagination.html.twig");
        echo "
                </div> <!-- .panel-body -->
            </div> <!-- .panel -->
        </div> <!-- .col-md-12 -->
    </div> <!-- .row -->
</div> <!-- .container -->
";
    }

    public function getTemplateName()
    {
        return "CommonUserBundle:Admin:index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  132 => 53,  126 => 49,  116 => 45,  111 => 43,  107 => 42,  101 => 41,  97 => 40,  94 => 39,  91 => 38,  87 => 37,  79 => 32,  75 => 31,  71 => 30,  66 => 29,  50 => 8,  47 => 7,  40 => 4,  37 => 3,  11 => 1,);
    }
}
