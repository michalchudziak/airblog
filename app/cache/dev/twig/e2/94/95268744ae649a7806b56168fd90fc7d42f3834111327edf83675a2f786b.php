<?php

/* AirBlogBundle:Posts:postsList.html.twig */
class __TwigTemplate_e29495268744ae649a7806b56168fd90fc7d42f3834111327edf83675a2f786b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        try {
            $this->parent = $this->env->loadTemplate("AirBlogBundle::base.html.twig");
        } catch (Twig_Error_Loader $e) {
            $e->setTemplateFile($this->getTemplateName());
            $e->setTemplateLine(1);

            throw $e;
        }

        $this->blocks = array(
            'pageTitle' => array($this, 'block_pageTitle'),
            'topImage' => array($this, 'block_topImage'),
            'contentTitle' => array($this, 'block_contentTitle'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "AirBlogBundle::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_pageTitle($context, array $blocks = array())
    {
        echo twig_escape_filter($this->env, (isset($context["listTitle"]) ? $context["listTitle"] : $this->getContext($context, "listTitle")), "html", null, true);
    }

    // line 5
    public function block_topImage($context, array $blocks = array())
    {
        // line 6
        echo "    <div class=\"slider\">
        <div class=\"viewport\">
            <div class=\"slides-container\">
                <div style=\"background-image: url('";
        // line 9
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/airblog/img/slide-1.jpg"), "html", null, true);
        echo "')\" class=\"slide\"></div>
                <div style=\"background-image: url('";
        // line 10
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/airblog/img/slide-2.jpg"), "html", null, true);
        echo "')\" class=\"slide\"></div>
                <div style=\"background-image: url('";
        // line 11
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/airblog/img/slide-3.jpg"), "html", null, true);
        echo "')\" class=\"slide\"></div>
            </div>
        </div>

        <span class=\"nav prev\"></span>
        <span class=\"nav next\"></span>
    </div>
";
    }

    // line 20
    public function block_contentTitle($context, array $blocks = array())
    {
        echo twig_escape_filter($this->env, (isset($context["listTitle"]) ? $context["listTitle"] : $this->getContext($context, "listTitle")), "html", null, true);
    }

    // line 22
    public function block_content($context, array $blocks = array())
    {
        // line 23
        echo "<div class=\"left-side\">
         
    ";
        // line 25
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["pagination"]) ? $context["pagination"] : $this->getContext($context, "pagination")));
        foreach ($context['_seq'] as $context["_key"] => $context["post"]) {
            // line 26
            echo "    <article class=\"post\">
        <header>
            <h1><a href=\"";
            // line 28
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("blog_post", array("slug" => $this->getAttribute($context["post"], "slug", array()))), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->getAttribute($context["post"], "title", array()), "html", null, true);
            echo "</a></h1>

            <div class=\"meta-data\">
                <div class=\"author\">Autor <a href=\"#\">";
            // line 31
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["post"], "author", array()), "username", array()), "html", null, true);
            echo "</a></div>
                <div class=\"create-date\">Dodano ";
            // line 32
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($context["post"], "publishedDate", array()), "d.m.Y, H:m"), "html", null, true);
            echo "</div>
            </div>
        </header>

        <div class=\"thumbnail\">
            <div class=\"meta-data\"> 
                <div class=\"categories\">
                    Kategoria:
                    <a href=\"";
            // line 40
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("blog_category", array("slug" => $this->getAttribute($this->getAttribute($context["post"], "category", array()), "slug", array()))), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["post"], "category", array()), "name", array()), "html", null, true);
            echo "</a>
                </div>

                <div class=\"tags\">
                    Tagi:
                    ";
            // line 45
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute($context["post"], "tags", array()));
            foreach ($context['_seq'] as $context["_key"] => $context["tag"]) {
                // line 46
                echo "                    <a href=\"";
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("blog_tag", array("slug" => $this->getAttribute($context["tag"], "slug", array()))), "html", null, true);
                echo "\">";
                echo twig_escape_filter($this->env, $this->getAttribute($context["tag"], "name", array()), "html", null, true);
                echo "</a>
                    ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['tag'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 48
            echo "                </div>
            </div>

            <img src=\"";
            // line 51
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl($this->getAttribute($context["post"], "thumbnail", array())), "html", null, true);
            echo "\" alt=\"\">
        </div>

            ";
            // line 54
            echo $this->env->getExtension('air_blog_extension')->shorten($this->getAttribute($context["post"], "content", array()), 300);
            echo "

        <footer>
            <a class=\"comments\" href=\"#\">Komentarzy (";
            // line 57
            echo twig_escape_filter($this->env, twig_length_filter($this->env, $this->getAttribute($context["post"], "comments", array())), "html", null, true);
            echo ")</a>
            <a href=\"";
            // line 58
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("blog_post", array("slug" => $this->getAttribute($context["post"], "slug", array()))), "html", null, true);
            echo "\" class=\"btn-green\">Czytaj całość [...]</a>
        </footer>
    </article> <!-- .post -->
    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['post'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 62
        echo "
    ";
        // line 63
        echo $this->env->getExtension('knp_pagination')->render((isset($context["pagination"]) ? $context["pagination"] : $this->getContext($context, "pagination")), "AirBlogBundle:Pagination:pagination.html.twig");
        echo "

</div> <!-- .left-side -->
";
    }

    public function getTemplateName()
    {
        return "AirBlogBundle:Posts:postsList.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  176 => 63,  173 => 62,  163 => 58,  159 => 57,  153 => 54,  147 => 51,  142 => 48,  131 => 46,  127 => 45,  117 => 40,  106 => 32,  102 => 31,  94 => 28,  90 => 26,  86 => 25,  82 => 23,  79 => 22,  73 => 20,  61 => 11,  57 => 10,  53 => 9,  48 => 6,  45 => 5,  39 => 3,  11 => 1,);
    }
}
