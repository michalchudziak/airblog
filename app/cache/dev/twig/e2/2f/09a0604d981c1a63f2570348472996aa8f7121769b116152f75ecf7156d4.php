<?php

/* AirAdminBundle:Posts:index.html.twig */
class __TwigTemplate_e22f09a0604d981c1a63f2570348472996aa8f7121769b116152f75ecf7156d4 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        try {
            $this->parent = $this->env->loadTemplate("AirAdminBundle::base.html.twig");
        } catch (Twig_Error_Loader $e) {
            $e->setTemplateFile($this->getTemplateName());
            $e->setTemplateLine(1);

            throw $e;
        }

        $this->blocks = array(
            'pageTitle' => array($this, 'block_pageTitle'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "AirAdminBundle::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_pageTitle($context, array $blocks = array())
    {
        // line 4
        echo "\t";
        $this->displayParentBlock("pageTitle", $context, $blocks);
        echo " - Posty
";
    }

    // line 7
    public function block_content($context, array $blocks = array())
    {
        // line 8
        echo "<div class=\"container\">
    <div class=\"row\">
        <div class=\"col-md-12\">

            <div class=\"panel panel-default\">
                <div class=\"panel-heading\">
                    Wyszukiwanie i filtracja
                </div>
                <div class=\"panel-body\">
                    <form action=\"";
        // line 17
        echo $this->env->getExtension('routing')->getPath("admin_postsList");
        echo "\" method=\"get\" class=\"form-inline filter-search\" role=\"form\">
                        <div class=\"form-group\">
                            <input type=\"text\" name=\"titleLike\" value=\"";
        // line 19
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["queryParams"]) ? $context["queryParams"] : $this->getContext($context, "queryParams")), "titleLike", array(), "array"), "html", null, true);
        echo "\" class=\"form-control\" placeholder=\"Wyszukaj po tytule\">
                        </div>

                        <div class=\"form-group\">
                            <select name=\"categoryId\" class=\"select-block\" placeholder=\"Kategoria\">
                                <option></option>
                                <option ";
        // line 25
        echo ((( -1 == $this->getAttribute((isset($context["queryParams"]) ? $context["queryParams"] : $this->getContext($context, "queryParams")), "categoryId", array(), "array"))) ? ("selected=\"selected\"") : (""));
        echo " value=\"-1\">Brak kategorii</option>
                                ";
        // line 26
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["categoriesList"]) ? $context["categoriesList"] : $this->getContext($context, "categoriesList")));
        foreach ($context['_seq'] as $context["_key"] => $context["category"]) {
            // line 27
            echo "                                <option ";
            echo ((($this->getAttribute($context["category"], "id", array(), "array") == $this->getAttribute((isset($context["queryParams"]) ? $context["queryParams"] : $this->getContext($context, "queryParams")), "categoryId", array(), "array"))) ? ("selected=\"selected\"") : (""));
            echo " value=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($context["category"], "id", array(), "array"), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->getAttribute($context["category"], "name", array(), "array"), "html", null, true);
            echo "</option>
                                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['category'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 29
        echo "                            </select>
                        </div>

                        <div class=\"form-group\">
                            <button class=\"btn btn-primary\">Filtruj</button>
                        </div>

                        <div class=\"form-group pull-right limit\">
                            <label for=\"inputEmail1\" class=\"col-lg-5 control-label\">Wyników</label>
                            <div class=\"col-lg-7\">
                                <select name=\"limit\" class=\"select-block\" id=\"limit\" placeholder=\"Pokaż\">
                                    ";
        // line 40
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["limits"]) ? $context["limits"] : $this->getContext($context, "limits")));
        foreach ($context['_seq'] as $context["_key"] => $context["limit"]) {
            // line 41
            echo "                                        <option ";
            echo ((((isset($context["currLimit"]) ? $context["currLimit"] : $this->getContext($context, "currLimit")) == $context["limit"])) ? ("selected=\"selected\"") : (""));
            echo " value=\"";
            echo twig_escape_filter($this->env, $context["limit"], "html", null, true);
            echo "\">
                                            ";
            // line 42
            echo twig_escape_filter($this->env, $context["limit"], "html", null, true);
            echo "
                                        </option>
                                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['limit'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 45
        echo "                                    ";
        // line 51
        echo "                                </select>
                            </div>
                        </div>

                    </form>
                </div>
            </div>

            <div class=\"panel panel-default\">
                <div class=\"panel-heading\">
                        Posty
                </div>
                <div class=\"panel-body\">
                    
                    <ul class=\"nav nav-pills pull-left\">
                        ";
        // line 66
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["statusesList"]) ? $context["statusesList"] : $this->getContext($context, "statusesList")));
        foreach ($context['_seq'] as $context["key"] => $context["val"]) {
            // line 67
            echo "                            <li ";
            echo ((((isset($context["currStatus"]) ? $context["currStatus"] : $this->getContext($context, "currStatus")) == $context["val"])) ? ("class=\"active\"") : (""));
            echo ">
                                <a href=\"";
            // line 68
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("admin_postsList", array("status" => $context["val"])), "html", null, true);
            echo "\">
                                    ";
            // line 69
            echo twig_escape_filter($this->env, $context["key"], "html", null, true);
            echo " (";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["statistics"]) ? $context["statistics"] : $this->getContext($context, "statistics")), $context["val"], array(), "array"), "html", null, true);
            echo ")
                                </a>
                            </li>
                        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['key'], $context['val'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 73
        echo "                        
                        ";
        // line 79
        echo "                    </ul>

                    <ul class=\"nav nav-pills pull-right\">
                        <li><a href=\"";
        // line 82
        echo $this->env->getExtension('routing')->getPath("admin_postForm");
        echo "\">Dodaj nowy</a></li>
                    </ul>

                    <div class=\"clearfix\"></div>

                    <div style=\"margin-top: 20px;\" class=\"table-responsive\">
                        <table class=\"table table-striped table-bordered table-hover\">
                            <thead>
                                <tr>
                                    <th>";
        // line 91
        echo $this->env->getExtension('knp_pagination')->sortable((isset($context["pagination"]) ? $context["pagination"] : $this->getContext($context, "pagination")), "ID", "p.id");
        echo "</th>
                                    <th>";
        // line 92
        echo $this->env->getExtension('knp_pagination')->sortable((isset($context["pagination"]) ? $context["pagination"] : $this->getContext($context, "pagination")), "Tytuł", "p.title");
        echo "</th>
                                    <th>";
        // line 93
        echo $this->env->getExtension('knp_pagination')->sortable((isset($context["pagination"]) ? $context["pagination"] : $this->getContext($context, "pagination")), "Kategoria", "c.id");
        echo "</th>
                                    ";
        // line 94
        echo " 
                                    ";
        // line 97
        echo "                                    <th>Tagi</th>
                                    <th>";
        // line 98
        echo $this->env->getExtension('knp_pagination')->sortable((isset($context["pagination"]) ? $context["pagination"] : $this->getContext($context, "pagination")), "Autor", "a.id");
        echo "</th>
                                    <th>";
        // line 99
        echo $this->env->getExtension('knp_pagination')->sortable((isset($context["pagination"]) ? $context["pagination"] : $this->getContext($context, "pagination")), "Data utworzenia", "p.createDate");
        echo "</th>
                                    <th>";
        // line 100
        echo $this->env->getExtension('knp_pagination')->sortable((isset($context["pagination"]) ? $context["pagination"] : $this->getContext($context, "pagination")), "Data publikacji", "p.publishedDate");
        echo "</th>
                                    ";
        // line 104
        echo "                                    <th class=\"actions\">Akcje</th>
                                </tr>
                            </thead>
                            <tbody>
                                ";
        // line 108
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["pagination"]) ? $context["pagination"] : $this->getContext($context, "pagination")));
        $context['_iterated'] = false;
        foreach ($context['_seq'] as $context["_key"] => $context["post"]) {
            // line 109
            echo "                                <tr>
                                    <td>";
            // line 110
            echo twig_escape_filter($this->env, $this->getAttribute($context["post"], "id", array()), "html", null, true);
            echo "</td>
                                    <td><a href=\"";
            // line 111
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("admin_postForm", array("id" => $this->getAttribute($context["post"], "id", array()))), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->getAttribute($context["post"], "title", array()), "html", null, true);
            echo "</a></td>
                                    <td>
                                        ";
            // line 113
            if ((null === $this->getAttribute($context["post"], "category", array()))) {
                // line 114
                echo "                                            brak kategorii
                                        ";
            } else {
                // line 116
                echo "                                            ";
                echo twig_escape_filter($this->env, twig_capitalize_string_filter($this->env, $this->getAttribute($this->getAttribute($context["post"], "category", array()), "name", array())), "html", null, true);
                echo "
                                        ";
            }
            // line 118
            echo "                                    </td>
                                    <td>
                                        ";
            // line 120
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute($context["post"], "tags", array()));
            $context['_iterated'] = false;
            foreach ($context['_seq'] as $context["_key"] => $context["tag"]) {
                // line 121
                echo "                                        <span class=\"label label-info\">";
                echo twig_escape_filter($this->env, $this->getAttribute($context["tag"], "name", array()), "html", null, true);
                echo "</span>
                                        ";
                $context['_iterated'] = true;
            }
            if (!$context['_iterated']) {
                // line 123
                echo "                                        Brak tagów
                                        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['tag'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 125
            echo "                                    </td>
                                    <td>";
            // line 126
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["post"], "author", array()), "username", array()), "html", null, true);
            echo "</td>
                                    <td>
                                        ";
            // line 128
            echo twig_escape_filter($this->env, $this->env->getExtension('air_admin_extension')->adminFormatDate($this->getAttribute($context["post"], "createDate", array())), "html", null, true);
            echo "
                                    </td>
                                    <td>
                                        ";
            // line 131
            if ((null === $this->getAttribute($context["post"], "publishedDate", array()))) {
                // line 132
                echo "                                            nie zaplanowano publikacji
                                        ";
            } else {
                // line 134
                echo "                                            ";
                echo twig_escape_filter($this->env, $this->env->getExtension('air_admin_extension')->adminFormatDate($this->getAttribute($context["post"], "publishedDate", array())), "html", null, true);
                echo "
                                        ";
            }
            // line 136
            echo "                                    </td>
                                    <td>
                                        <div class=\"btn-group\">
                                            <a href=\"";
            // line 139
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("admin_postForm", array("id" => $this->getAttribute($context["post"], "id", array()))), "html", null, true);
            echo "\" class=\"btn btn-primary\">
                                                <span class=\"glyphicon glyphicon-edit\"></span>
                                            </a>
                                            
                                            ";
            // line 143
            if ($this->env->getExtension('security')->isGranted("ROLE_ADMIN")) {
                // line 144
                echo "                                                ";
                $context["tokenName"] = $this->getAttribute((isset($context["csrfProvider"]) ? $context["csrfProvider"] : $this->getContext($context, "csrfProvider")), "generateCsrfToken", array(0 => sprintf((isset($context["deleteTokenName"]) ? $context["deleteTokenName"] : $this->getContext($context, "deleteTokenName")), $this->getAttribute($context["post"], "id", array()))), "method");
                // line 145
                echo "                                                ";
                $context["deleteUrl"] = $this->env->getExtension('routing')->getPath("admin_postDelete", array("id" => $this->getAttribute($context["post"], "id", array()), "token" => (isset($context["tokenName"]) ? $context["tokenName"] : $this->getContext($context, "tokenName"))));
                // line 146
                echo "                                                <a href=\"";
                echo twig_escape_filter($this->env, (isset($context["deleteUrl"]) ? $context["deleteUrl"] : $this->getContext($context, "deleteUrl")), "html", null, true);
                echo "\" class=\"btn btn-danger\" data-confirmAction=\"\">
                                                    <span class=\"glyphicon glyphicon-trash\"></span>
                                                </a>
                                            ";
            }
            // line 150
            echo "                                        </div>
                                    </td>
                                </tr>
                                ";
            $context['_iterated'] = true;
        }
        if (!$context['_iterated']) {
            // line 154
            echo "                                <tr>
                                    <td colspan=\"8\">Brak dopasowanych postów</td>
                                </tr>
                                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['post'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 158
        echo "                            </tbody>
                        </table>
                    </div>
                    
                    ";
        // line 162
        echo $this->env->getExtension('knp_pagination')->render((isset($context["pagination"]) ? $context["pagination"] : $this->getContext($context, "pagination")), "AirAdminBundle:Pagination:admin_pagination.html.twig");
        echo "
                </div> <!-- .panel-body -->
            </div> <!-- .panel -->
        </div> <!-- .col-md-12 -->
    </div> <!-- .row -->
</div> <!-- .container -->
";
    }

    public function getTemplateName()
    {
        return "AirAdminBundle:Posts:index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  361 => 162,  355 => 158,  346 => 154,  338 => 150,  330 => 146,  327 => 145,  324 => 144,  322 => 143,  315 => 139,  310 => 136,  304 => 134,  300 => 132,  298 => 131,  292 => 128,  287 => 126,  284 => 125,  277 => 123,  269 => 121,  264 => 120,  260 => 118,  254 => 116,  250 => 114,  248 => 113,  241 => 111,  237 => 110,  234 => 109,  229 => 108,  223 => 104,  219 => 100,  215 => 99,  211 => 98,  208 => 97,  205 => 94,  201 => 93,  197 => 92,  193 => 91,  181 => 82,  176 => 79,  173 => 73,  161 => 69,  157 => 68,  152 => 67,  148 => 66,  131 => 51,  129 => 45,  120 => 42,  113 => 41,  109 => 40,  96 => 29,  83 => 27,  79 => 26,  75 => 25,  66 => 19,  61 => 17,  50 => 8,  47 => 7,  40 => 4,  37 => 3,  11 => 1,);
    }
}
