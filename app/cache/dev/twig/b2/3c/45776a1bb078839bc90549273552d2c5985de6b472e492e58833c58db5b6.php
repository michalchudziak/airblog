<?php

/* AirBlogBundle:Posts:post.html.twig */
class __TwigTemplate_b23c45776a1bb078839bc90549273552d2c5985de6b472e492e58833c58db5b6 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        try {
            $this->parent = $this->env->loadTemplate("AirBlogBundle::base.html.twig");
        } catch (Twig_Error_Loader $e) {
            $e->setTemplateFile($this->getTemplateName());
            $e->setTemplateLine(1);

            throw $e;
        }

        $this->blocks = array(
            'pageTitle' => array($this, 'block_pageTitle'),
            'contentTitle' => array($this, 'block_contentTitle'),
            'content' => array($this, 'block_content'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "AirBlogBundle::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 93
        if ($this->env->getExtension('security')->isGranted("ROLE_ADMIN")) {
        }
        // line 1
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_pageTitle($context, array $blocks = array())
    {
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["post"]) ? $context["post"] : $this->getContext($context, "post")), "title", array()), "html", null, true);
    }

    // line 5
    public function block_contentTitle($context, array $blocks = array())
    {
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["post"]) ? $context["post"] : $this->getContext($context, "post")), "title", array()), "html", null, true);
    }

    // line 7
    public function block_content($context, array $blocks = array())
    {
        // line 8
        echo "<div class=\"left-side\">
    
    ";
        // line 10
        $this->env->loadTemplate("AirBlogBundle:Template:flashMsg.html.twig")->display($context);
        // line 11
        echo "
    <article class=\"post\">
        <header>
            <h1>";
        // line 14
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["post"]) ? $context["post"] : $this->getContext($context, "post")), "title", array()), "html", null, true);
        echo "</h1>

            <div class=\"meta-data\">
                <div class=\"author\">Autor <a href=\"#\">";
        // line 17
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["post"]) ? $context["post"] : $this->getContext($context, "post")), "author", array()), "username", array()), "html", null, true);
        echo "</a></div>
                <div class=\"create-date\">Dodano ";
        // line 18
        echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute((isset($context["post"]) ? $context["post"] : $this->getContext($context, "post")), "publishedDate", array()), "d.m.Y, H:m"), "html", null, true);
        echo "</div>
            </div>
        </header>

        <div class=\"thumbnail\">
            <div class=\"meta-data\">
                <div class=\"categories\">
                    Kategoria:
                    <a href=\"";
        // line 26
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("blog_category", array("slug" => $this->getAttribute($this->getAttribute((isset($context["post"]) ? $context["post"] : $this->getContext($context, "post")), "category", array()), "slug", array()))), "html", null, true);
        echo "\">";
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["post"]) ? $context["post"] : $this->getContext($context, "post")), "category", array()), "name", array()), "html", null, true);
        echo "</a>
                </div>

                <div class=\"tags\">
                    Tagi:
                    ";
        // line 31
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["post"]) ? $context["post"] : $this->getContext($context, "post")), "tags", array()));
        foreach ($context['_seq'] as $context["_key"] => $context["tag"]) {
            // line 32
            echo "                    <a href=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("blog_tag", array("slug" => $this->getAttribute($context["tag"], "slug", array()))), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->getAttribute($context["tag"], "name", array()), "html", null, true);
            echo "</a>
                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['tag'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 34
        echo "                </div>
            </div>

            ";
        // line 38
        echo "            <img src=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl($this->getAttribute((isset($context["post"]) ? $context["post"] : $this->getContext($context, "post")), "thumbnail", array())), "html", null, true);
        echo "\" alt=\"\">
        </div>

        ";
        // line 41
        echo $this->getAttribute((isset($context["post"]) ? $context["post"] : $this->getContext($context, "post")), "content", array());
        echo "
    </article>

    <section class=\"post-comments\" id=\"post-comments\">
        <header>
            <h3>Komentarze (";
        // line 46
        echo twig_escape_filter($this->env, twig_length_filter($this->env, $this->getAttribute((isset($context["post"]) ? $context["post"] : $this->getContext($context, "post")), "comments", array())), "html", null, true);
        echo ")</h3>
        </header>

        ";
        // line 49
        if ($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "user", array())) {
            // line 50
            echo "        <div class=\"comment\">
            <img class=\"thumbnail\" src=\"";
            // line 51
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "user", array()), "avatar", array())), "html", null, true);
            echo "\" alt=\"\">

            <div class=\"body\">
                <div class=\"author\">";
            // line 54
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "user", array()), "username", array()), "html", null, true);
            echo "</div>
                <div class=\"create-date\">";
            // line 55
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_date_converter($this->env), "d/m/Y"), "html", null, true);
            echo "</div>

                <div class=\"clearfix\"></div>
                
                ";
            // line 59
            $this->env->getExtension('form')->renderer->setTheme((isset($context["commentForm"]) ? $context["commentForm"] : $this->getContext($context, "commentForm")), array(0 => "AirBlogBundle:Form:form_template.html.twig"));
            // line 60
            echo "                ";
            echo             $this->env->getExtension('form')->renderer->renderBlock((isset($context["commentForm"]) ? $context["commentForm"] : $this->getContext($context, "commentForm")), 'form', array("action" => "#post-comments"));
            echo "
            </div>
        </div>
        ";
        }
        // line 64
        echo "
        ";
        // line 65
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["post"]) ? $context["post"] : $this->getContext($context, "post")), "comments", array()));
        foreach ($context['_seq'] as $context["_key"] => $context["comment"]) {
            // line 66
            echo "        <div class=\"comment\">
            <img class=\"thumbnail\" src=\"";
            // line 67
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl($this->getAttribute($this->getAttribute($context["comment"], "author", array()), "avatar", array())), "html", null, true);
            echo "\" alt=\"\">

            <div class=\"body\">
                <div class=\"author\">
                    ";
            // line 71
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["comment"], "author", array()), "username", array()), "html", null, true);
            echo "
                    
                    ";
            // line 73
            if ($this->env->getExtension('security')->isGranted("ROLE_ADMIN")) {
                // line 74
                echo "                    <div class=\"divider\"></div>
                    ";
                // line 75
                $context["commentToken"] = $this->getAttribute((isset($context["csrfProvider"]) ? $context["csrfProvider"] : $this->getContext($context, "csrfProvider")), "generateCsrfToken", array(0 => sprintf((isset($context["tokenName"]) ? $context["tokenName"] : $this->getContext($context, "tokenName")), $this->getAttribute($context["comment"], "id", array()))), "method");
                // line 76
                echo "                    ";
                $context["deleteUrl"] = $this->env->getExtension('routing')->getPath("blog_deleteComment", array("commentId" => $this->getAttribute($context["comment"], "id", array()), "token" => (isset($context["commentToken"]) ? $context["commentToken"] : $this->getContext($context, "commentToken"))));
                // line 77
                echo "                    <a href=\"";
                echo twig_escape_filter($this->env, (isset($context["deleteUrl"]) ? $context["deleteUrl"] : $this->getContext($context, "deleteUrl")), "html", null, true);
                echo "\" class=\"delete delete-comment\">Usuń</a>
                    ";
            }
            // line 79
            echo "                </div>
                <div class=\"create-date\">";
            // line 80
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($context["comment"], "createDate", array()), "d/m/Y"), "html", null, true);
            echo "</div>

                <div class=\"clearfix\"></div>

                <p>";
            // line 84
            echo twig_escape_filter($this->env, $this->getAttribute($context["comment"], "comment", array()), "html", null, true);
            echo "</p>
            </div>
        </div>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['comment'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 88
        echo "    </section>

</div> <!-- .left -->
";
    }

    // line 94
    public function block_javascripts($context, array $blocks = array())
    {
        // line 95
        echo "        ";
        $this->displayParentBlock("javascripts", $context, $blocks);
        echo "
        
        <script>
            \$(document).ready(function(){
                
                \$('body').on('click', '.delete-comment', function(e){
                    e.preventDefault();
                    
                    var \$btn = \$(this);
                    var deleteUrl = \$btn.attr('href');
                    
                    \$.getJSON(deleteUrl, function(json){
                        alert(json.message);
                        
                        if('ok' === json.status){
                            \$btn.closest('.comment').remove();
                        }
                    });
                });
                
            });
            
        </script>
    ";
    }

    public function getTemplateName()
    {
        return "AirBlogBundle:Posts:post.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  241 => 95,  238 => 94,  231 => 88,  221 => 84,  214 => 80,  211 => 79,  205 => 77,  202 => 76,  200 => 75,  197 => 74,  195 => 73,  190 => 71,  183 => 67,  180 => 66,  176 => 65,  173 => 64,  165 => 60,  163 => 59,  156 => 55,  152 => 54,  146 => 51,  143 => 50,  141 => 49,  135 => 46,  127 => 41,  120 => 38,  115 => 34,  104 => 32,  100 => 31,  90 => 26,  79 => 18,  75 => 17,  69 => 14,  64 => 11,  62 => 10,  58 => 8,  55 => 7,  49 => 5,  43 => 3,  39 => 1,  36 => 93,  11 => 1,);
    }
}
