<?php

/* AirAdminBundle:Dashboard:index.html.twig */
class __TwigTemplate_14741d06149953aba448aa2b37ed0f6ee80a04f98d09a0890a12fd4eb4a5ace2 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        try {
            $this->parent = $this->env->loadTemplate("AirAdminBundle::base.html.twig");
        } catch (Twig_Error_Loader $e) {
            $e->setTemplateFile($this->getTemplateName());
            $e->setTemplateLine(1);

            throw $e;
        }

        $this->blocks = array(
            'pageTitle' => array($this, 'block_pageTitle'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "AirAdminBundle::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_pageTitle($context, array $blocks = array())
    {
        // line 4
        echo "\t";
        $this->displayParentBlock("pageTitle", $context, $blocks);
        echo " - Kokpit
";
    }

    // line 7
    public function block_content($context, array $blocks = array())
    {
        // line 8
        echo "    <div class=\"container\">
        <div class=\"jumbotron\">
            <h1>Witaj!</h1>
            <p>Użyj górnego menu, aby poruszać się po Panelu lub użyj przycisków poniżej, aby przyspieszyć swoją pracę.</p>
            <p><a href=\"";
        // line 12
        echo $this->env->getExtension('routing')->getPath("admin_postForm");
        echo "\" class=\"btn btn-primary btn-lg\">Dodaj nowy post</a></p>
        </div>
    </div>
";
    }

    public function getTemplateName()
    {
        return "AirAdminBundle:Dashboard:index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  56 => 12,  50 => 8,  47 => 7,  40 => 4,  37 => 3,  11 => 1,);
    }
}
