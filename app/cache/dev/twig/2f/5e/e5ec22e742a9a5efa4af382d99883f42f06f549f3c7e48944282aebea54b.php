<?php

/* AirAdminBundle:Tags:form.html.twig */
class __TwigTemplate_2f5ee5ec22e742a9a5efa4af382d99883f42f06f549f3c7e48944282aebea54b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        try {
            $this->parent = $this->env->loadTemplate("AirAdminBundle::base.html.twig");
        } catch (Twig_Error_Loader $e) {
            $e->setTemplateFile($this->getTemplateName());
            $e->setTemplateLine(1);

            throw $e;
        }

        $this->blocks = array(
            'pageTitle' => array($this, 'block_pageTitle'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "AirAdminBundle::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 4
    public function block_pageTitle($context, array $blocks = array())
    {
        // line 5
        echo "    ";
        if ((null === $this->getAttribute((isset($context["tag"]) ? $context["tag"] : $this->getContext($context, "tag")), "id", array()))) {
            // line 6
            echo "        ";
            $this->displayParentBlock("pageTitle", $context, $blocks);
            echo " - Dodawanie nowego Tagu
    ";
        } else {
            // line 8
            echo "        ";
            $this->displayParentBlock("pageTitle", $context, $blocks);
            echo " - Edycja tagu \"";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["tag"]) ? $context["tag"] : $this->getContext($context, "tag")), "name", array()), "html", null, true);
            echo "\"
    ";
        }
    }

    // line 12
    public function block_content($context, array $blocks = array())
    {
        // line 13
        echo "<div class=\"container\">
    <div class=\"row\">
        <div class=\"col-md-12\">
            <div class=\"panel panel-default\">
                <div class=\"panel-heading\">
                    ";
        // line 18
        if ((null === $this->getAttribute((isset($context["tag"]) ? $context["tag"] : $this->getContext($context, "tag")), "id", array()))) {
            // line 19
            echo "                        Dodawanie nowego Tagu
                    ";
        } else {
            // line 21
            echo "                        Edycja tagu \"";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["tag"]) ? $context["tag"] : $this->getContext($context, "tag")), "name", array()), "html", null, true);
            echo "\"
                    ";
        }
        // line 23
        echo "                </div>
                <div class=\"panel-body\">
                    ";
        // line 25
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'form_start');
        echo "
                        ";
        // line 26
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "name", array()), 'row');
        echo "
                        ";
        // line 27
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "slug", array()), 'row');
        echo "
                        ";
        // line 28
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "save", array()), 'row', array("attr" => array("class" => "pull-right")));
        echo "
                    ";
        // line 29
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'form_end');
        echo "
                </div>
            </div> <!-- .panel -->
        </div> <!-- .col-md-12 -->                 
    </div> <!-- .row -->
</div> <!-- .container -->
";
    }

    public function getTemplateName()
    {
        return "AirAdminBundle:Tags:form.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  101 => 29,  97 => 28,  93 => 27,  89 => 26,  85 => 25,  81 => 23,  75 => 21,  71 => 19,  69 => 18,  62 => 13,  59 => 12,  49 => 8,  43 => 6,  40 => 5,  37 => 4,  11 => 1,);
    }
}
