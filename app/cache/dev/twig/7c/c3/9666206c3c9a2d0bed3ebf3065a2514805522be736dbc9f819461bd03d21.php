<?php

/* AirBlogBundle:Pages:contact.html.twig */
class __TwigTemplate_7cc39666206c3c9a2d0bed3ebf3065a2514805522be736dbc9f819461bd03d21 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        try {
            $this->parent = $this->env->loadTemplate("AirBlogBundle::base.html.twig");
        } catch (Twig_Error_Loader $e) {
            $e->setTemplateFile($this->getTemplateName());
            $e->setTemplateLine(1);

            throw $e;
        }

        $this->blocks = array(
            'pageTitle' => array($this, 'block_pageTitle'),
            'contentTitle' => array($this, 'block_contentTitle'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "AirBlogBundle::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_pageTitle($context, array $blocks = array())
    {
        echo "Kontakt";
    }

    // line 5
    public function block_contentTitle($context, array $blocks = array())
    {
        echo "Kontakt";
    }

    // line 7
    public function block_content($context, array $blocks = array())
    {
        // line 8
        echo "<div class=\"contact-page left-side\">

    ";
        // line 10
        $this->env->loadTemplate("AirBlogBundle:Template:flashMsg.html.twig")->display($context);
        // line 11
        echo "    
    ";
        // line 12
        $this->env->getExtension('form')->renderer->setTheme((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), array(0 => "AirBlogBundle:Form:form_template.html.twig"));
        // line 13
        echo "    ";
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'form');
        echo "
</div> <!-- .left -->
";
    }

    public function getTemplateName()
    {
        return "AirBlogBundle:Pages:contact.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  64 => 13,  62 => 12,  59 => 11,  57 => 10,  53 => 8,  50 => 7,  44 => 5,  38 => 3,  11 => 1,);
    }
}
